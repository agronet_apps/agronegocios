angular.module('starter')
    // se creo factori para la manupulacion de datos para que
    // funcione se inyecta el nombre en los controladores

    .factory('InfoService', ['$window', '$http','$ionicLoading', '$cordovaDialogs',function(
        $window,
        $http, 
        $ionicLoading,
        $cordovaDialogs, 
        $ionicPlatform){
		/*Definicion de variables*/
 var headers = {
'Access-Control-Allow-Origin' : '*',
'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
'Content-Type': 'application/json',
'Accept': 'application/json'
};
var URL=//"http://192.168.1.134:8081/api/agronegocios/";//"http://186.155.199.197:8000/api/agronegocios/"//"http://192.168.1.134:8081/api/agronegocios/";//186.155.199.197:8000
var URLGeo="http://192.168.1.134:8081/api/agronegocios/";//"http://186.155.199.197:8000/api/";
var customers = {};
var detail = {};
var app_id="461d261e";
var api_key="817e75a8fa18a7c69b9255574f0c34e6aadd259dbb26f908";
var gcm_key="gcm_key <your-gcm-project-number>";
/*SERVICIOS UTILIZADOS POR LA APP FUNCIONANDO*/


.service('GetCompradores', function($http, $log) {
    this.getBuyers = function(municipio,desde) {
		$cordovaDialogs.alert(
					    'Error en la conexion', 
					    'Error', 
					    'Ok')
       var promise  = $http({
           method : 'POST',
           url : URL+'CompraDetalle',
           data: { "MunicipioId":municipio,"Desde":desde},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO COMPRA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
		   alert('Error de conexión, por favor intente nuevamente');
		   /*$cordovaDialogs.alert(
					    'Error en la conexion', 
					    'Error', 
					    'Ok')*/
       }); 
       return promise; 
    };
})
.service('GetVendedores', function($http, $log) {
    this.getVende = function(municipio,desde) {
       //var toSend={ "MunicipioId":municipio,"Desde":desde}
        //   console.log("JSON TO SEND : "+angular.toJson(toSend)) 
       var promise  = $http({
           method : 'POST',
           url : URL+'VentaDetalle',
           data: { "MunicipioId":municipio,"Desde":desde},
           headers: headers,

       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO VENTA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('FunSipsa', function($http, $log) {
    this.getSipsa = function(municipio,producto) {
       var toSend={ "Producto":producto,"Municipio":municipio};
           console.log("JSON TO SEND FUNSIPSA: "+angular.toJson(toSend)) ;
       var promise  = $http({
           method : 'POST',
           url : URL+'FunSipsa',
           data: { "Producto":producto,"Municipio":municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO  FUNSIPSA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('PreciosFinca', function($http, $log) {//Change
    this.getSipsa = function(municipio) {
       var toSend={ "Id":municipio}
           console.log("JSON TO SEND : "+angular.toJson(toSend)) 
       var promise  = $http({
           method : 'POST',
           url : URL+'PreciosFinca',
           data: { "Id":municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO FINCA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('PreciosSipsa', function($http, $log) {
    this.getSipsa = function(municipio,producto) {
       var toSend={ "Producto":producto,"Municipio":municipio}
           console.log("JSON TO SEND : "+angular.toJson(toSend)) 
       var promise  = $http({
           method : 'POST',
           url : URL+'PreciosSipsa',
           data: { "Producto":producto,"Municipio":municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO SIPSA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('LogIn', function($http, $log) {
    this.logIn = function(usuario,clave) {
       var promise  = $http({
           method : 'POST',
           url : URL+'IniciarSesion',
           data: { "Email":usuario,"Password":clave,"AppId":app_id,"TokenPush":gcm_key},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('RegistroLogin', function($http, $log) {
    this.register = function(usuario,clave) {
       console.log("JSON A ENVIAR : "+usuario+"/ "+clave)
       var promise  = $http({
           method : 'POST',
           url : URL+'Registro',
           data: { "Correo":usuario,"Clave":clave},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerListasPerfil', function($http, $log) {
    this.register = function(usuario,clave) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerListasPerfil',
           //data: { "Email":usuario,"Password":clave,"Llave":app_id},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerListasPerfilCategorias', function($http, $log) {
    this.register = function(usuario,clave) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerListas',
           //data: { "Email":usuario,"Password":clave,"Llave":app_id},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('completarPerfil', function($http, $log) {
    this.update = function(UserId,Nombre,SegundoNombre,Apellido,Correo,DocIdentidad,NivelEducativo,IdMunicipio,Direccion,Genero,NumeroCelular,LugarDeProduccion,PerteneceAsociacion,Asociacion,NitAsociacion,IdActividadAgricola,IdCategoriaProducto) {
      console.log("JSON A ENVIAR : "+UserId+" /"+Nombre+" /"+SegundoNombre+" /"+Apellido+" /"+Correo+" /"+DocIdentidad+" /"+NivelEducativo+" /"+IdMunicipio+" /"+Direccion+" /"+Genero+" /"+NumeroCelular+" /"+LugarDeProduccion+" /"+PerteneceAsociacion+" /"+Asociacion+" /"+NitAsociacion+" /"+IdActividadAgricola+" /"+IdCategoriaProducto)
       var promise  = $http({
           method : 'POST',
           url : URL+'ActualizarPerfil',
           data: { "UserId":UserId,"Nombre":Nombre,"SegundoNombre":SegundoNombre,"Apellido":Apellido,"Correo":Correo,"DocIdentidad":DocIdentidad,"NivelEducativo":NivelEducativo,"IdMunicipio":IdMunicipio,"Direccion":Direccion,"Genero":Genero,"LugarDeProduccion":LugarDeProduccion,"PerteneceAsociacion":PerteneceAsociacion,"Asociacion":Asociacion,"NitAsociacion":NitAsociacion,"IdActividadAgricola":IdActividadAgricola,"IdCategoriaProducto":IdCategoriaProducto},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('crearCompra', function($http, $log) {
    this.create = function(Cantidad,Descripcion,Direccion,FechaEntrega,UnidadMedida,IdCiudad,PresentacionCultivo,IdUsuario,Variedad,Estado,Republicar,Bonificacion,Latitud,Longitud) {
           var toSend={ "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud}
           console.log("JSON TO SEND : "+angular.toJson(toSend))         
           var promise  = $http({
           method : 'POST',
           url : URL+'CrearCompra',
           data: { "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('editarCompra', function($http, $log) {
    this.update = function(Cantidad,Descripcion,Direccion,FechaEntrega,UnidadMedida,IdCiudad,PresentacionCultivo,IdUsuario,Variedad,Estado,Republicar,Bonificacion,Latitud,Longitud) {
           var toSend={ "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud}
           console.log("JSON TO SEND : "+angular.toJson(toSend))         
           var promise  = $http({
           method : 'POST',
           url : URL+'EditarVenta',
           data: { "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('removeOferta', function($http, $log) {
    this.remove = function(Id,token,state) {
        var toSend={ "Id":Id,"Token":token,"Estado":state}
           console.log("JSON TO SEND : "+angular.toJson(toSend))  
       var promise  = $http({
           method : 'POST',
           url : URL+'QuitarNegocio',
           data: { "Id":Id,"Token":token,"Estado":state},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('crearVenta', function($http, $log) {
       this.createventa = function(Cantidad,PrecioUnitario,VigenciaDesde,VigenciaHasta,Descripcion,FechaCreacion,FechaActualizacion,Republicar,Latitud,Longitud,Token,Variedad,Estado,UnidadMedida,Presentacion,Municipio) { 
       var toSend={ "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio}
       console.log("JSON TO SEND : "+angular.toJson(toSend))     
       var promise  = $http({
           method : 'POST',
           url : URL+'AgregarVenta',
           data: { "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('editarVenta', function($http, $log) {
       this.editar = function(Cantidad,PrecioUnitario,VigenciaDesde,VigenciaHasta,Descripcion,FechaCreacion,FechaActualizacion,Republicar,Latitud,Longitud,Token,Variedad,Estado,UnidadMedida,Presentacion,Municipio) { 
       var toSend={ "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio}
       console.log("JSON TO SEND : "+angular.toJson(toSend))     
       var promise  = $http({
           method : 'POST',
           url : URL+'EditarVenta',
           data: { "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerVariedades', function($http, $log) {
    this.get = function(CategoryId,query) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerVariedades',
           data: { "CategoryId":CategoryId,"Query":query},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('saveFavorito', function($http, $log) {
    this.save = function(Id,token,estate) {
       var toSend={ "Id":Id,"Token":token,"Estado":estate}
       console.log("JSON TO SEND : "+angular.toJson(toSend))
       var promise  = $http({
           method : 'POST',
           url : URL+'AgregarFavoritoNegocios',
           data: { "Id":Id,"Token":token,"Estado":estate},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('removeFavorito', function($http, $log) {
    this.remove = function(Id,token,state) {
       var promise  = $http({
           method : 'POST',
           url : URL+'QuitarFavoritoNegocios',
           data: { "Id":Id,"Token":token,"Estado":state},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('getFavoritos', function($http, $log) {
    this.get = function(token) {
       var promise  = $http({
           method : 'POST',
           url : URL+'FavoritosCompra',
           data: { "Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO FavoritosCompra: "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('getFavoritosVenta', function($http, $log) {
    this.get = function(token) {
       var promise  = $http({
           method : 'POST',
           url : URL+'FavoritosVenta',
           data: {"Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO FavoritosVenta: "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('publicacionesVenta', function($http, $log) {
    this.get = function(token) {
       var promise  = $http({
           method : 'POST',
           url : URL+'MisPublicacionesVenta',
           data: { "Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('publicacionesCompra', function($http, $log) {
    this.get = function(token) {
       var promise  = $http({
           method : 'POST',
           url : URL+'MisPublicacionesCompra',
           data: { "Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('OlvidoClave', function($http, $log) {
    this.forgot = function(correo) {
       var promise  = $http({
           method : 'POST',
           url : URL+'Olvido',
           data: { "Email":correo},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerPerfil', function($http, $log) {
    this.get = function(id) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerPerfil',
           data: { "UserId":id},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetDetail', function($http, $log) {
    this.getBuyer = function(idCompra) { 
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerCompra',
           data: { "Id":idCompra},
           headers: headers,
       }).success(function(data, status, headers, config) {
           detail = data.Data;
           return detail;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetCityDefault', function($http, $log) {
    this.get = function(lat,lon) { 
       var promise  = $http({
           method : 'GET',
           url : URLGeo+'AgroInsumo/GetLocation?Long='+lon+'&Lat='+lat,
           headers: headers,
       }).success(function(data, status, headers, config) {
           detail = data.Data;
           return detail;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetMunicipios', function($http, $log) {
    this.getMunis = function(query) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerMunicipios',
           data: { "query":query},
           headers: headers,
       }).success(function(data, status, headers, config) {
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('CambiarClave', function($http, $log) {
    this.update = function(token,nueva,confirmacion) {
      var toSend={ "UserId":token,"Nueva":nueva,"Confirmacion":confirmacion}
       console.log("JSON TO SEND : "+angular.toJson(toSend))
       var promise  = $http({
           method : 'POST',
           url : URL+'CambiarContrasena',
           data: { "UserId":token,"Nueva":nueva,"Confirmacion":confirmacion},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
});	
			
		}])
