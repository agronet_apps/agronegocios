 
/*Definicion de variables*/
 var headers = {
'Access-Control-Allow-Origin' : '*',
'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
'Content-Type': 'application/json',
'Accept': 'application/json'
};
var URL="http://104.196.130.110/saviaWebApi/api/agronegocios/";
//var URL="http://186.155.199.197:8000/api/agronegocios/";//"http://192.168.1.134:8081/api/agronegocios/";//"http://186.155.199.197:8000/api/agronegocios/"//"http://serviciosmadr.minagricultura.gov.co/saviawebapi/api/agronegocios/";//"http://186.155.199.197:8000/api/agronegocios/"//"http://192.168.1.134:8081/api/agronegocios/";//186.155.199.197:8000
var URLGeo="http://104.196.130.110/saviaWebApi/api/";//"http://186.155.199.197:8000/api/";//"http://192.168.1.134:8081/api/";//
var customers = {};
var detail = {};
var app_id="461d261e";
var api_key="817e75a8fa18a7c69b9255574f0c34e6aadd259dbb26f908";
var gcm_key="gcm_key <your-gcm-project-number>";
/*SERVICIOS UTILIZADOS POR LA APP FUNCIONANDO*/
angular.module('starter')

.service('GetCompradores', function($http, $log) {
     
    this.getBuyers = function(municipio,desde, producto) {
          $('#municipiolupa').hide();
          $('#munloader').show();
          var toSend={ "MunicipioId":municipio,"Desde":desde, "Producto": producto};
          console.log("JSON TO SEND : "+angular.toJson(toSend)) 
       var promise  = $http({
           method : 'POST',
           url : URL+'CompraDetalle',
           data: { "MunicipioId":municipio,"Desde":desde, "Producto": producto},
           headers: headers,
       }).success(function(data, status, headers, config) {
          $('#municipiolupa').show();
          $('#munloader').hide();
           console.log("JSON CREADO COMPRA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
          $('#municipiolupa').show();
          $('#munloader').hide();
          // return response;
		    //  console.log('Error de conexión, por favor intente nuevamente');
        //  return status;
		   /*$cordovaDialogs.alert(
					    'Error en la conexion', 
					    'Error', 
					    'Ok')*/
       }); 
      
       return promise; 
    };
})
.service('GetVendedores', function($http, $log) {
        
    this.getVende = function(municipio,desde, producto) {
        $('#municipiolupa').hide();
          $('#munloader').show();
        var toSend={ "MunicipioId":municipio,"Desde":desde, "Producto": producto};
           console.log("JSON TO SEND : "+angular.toJson(toSend)) 

       var promise  = $http({
           method : 'POST',
           url : URL+'VentaDetalle',
           data: { "MunicipioId":municipio,"Desde":desde, "Producto": producto},
           headers: headers,

       }).success(function(data, status, headers, config) {
          $('#municipiolupa').show();
          $('#munloader').hide();
           console.log("JSON CREADO VENTA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
          $('#municipiolupa').show();
          $('#munloader').hide();
       }); 
       return promise; 
    };
})
.service('FunSipsa', function($http, $log) {
    this.getSipsa = function(municipio,producto) {
       var toSend={ "Producto":producto,"Municipio":municipio};
           console.log("JSON TO SEND FUNSIPSA: "+angular.toJson(toSend)) ;
       var promise  = $http({
           method : 'POST',
           url : URL+'FunSipsa',
           data: { "Producto":producto,"Municipio":municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO  FUNSIPSA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('PreciosFinca', function($http, $log) {//Change
    this.getSipsa = function(municipio,producto,IdProducto) {
       var toSend={ "Municipio":municipio,"Producto":producto,"IdProducto":IdProducto}
           console.log("JSON TO SEND : "+angular.toJson(toSend)) 
       var promise  = $http({
           method : 'POST',
           url : URL+'PreciosFinca',
           data: { "Municipio":municipio,"Producto":producto,"IdProducto":IdProducto},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO FINCA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('PreciosSipsa', function($http, $log) {
    this.getSipsa = function(municipio,producto, CodProducto) {
       var toSend={ "Producto":producto,"Municipio":municipio, "CodProducto":CodProducto}
           console.log("JSON TO SEND : "+angular.toJson(toSend)) 
       var promise  = $http({
           method : 'POST',
           url : URL+'PreciosSipsa',
           data: { "Producto":producto,"Municipio":municipio, "CodProducto":CodProducto},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO SIPSA: "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('LogIn', function($http, $log) {
    this.logIn = function(usuario,clave) {
       var promise  = $http({
           method : 'POST',
           url : URL+'IniciarSesion',
           data: { "Email":usuario,"Password":clave,"AppId":app_id,"TokenPush":gcm_key},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data)) 
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('RegistroLogin', function($http, $log) {
    this.register = function(usuario,clave) {
       console.log("JSON A ENVIAR : "+usuario+"/ "+clave)
       var promise  = $http({
           method : 'POST',
           url : URL+'Registro',
           data: { "Correo":usuario,"Clave":clave},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerListasPerfil', function($http, $log) {
    this.register = function(usuario,clave) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerListasPerfil',
           //data: { "Email":usuario,"Password":clave,"Llave":app_id},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerListasPerfilCategorias', function($http, $log) {
    this.register = function(usuario,clave) {
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerListas',
           //data: { "Email":usuario,"Password":clave,"Llave":app_id},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('completarPerfil', function($http, $log) {
    this.update = function(UserId,Nombre,SegundoNombre,Apellido,Correo,DocIdentidad,NivelEducativo,IdMunicipio,Direccion,Genero,NumeroCelular,idLugarDeProduccion,PerteneceAsociacion,Asociacion,NitAsociacion,IdActividadAgricola,IdCategoriaProducto,superfice,productionPricipal, municipioProduccion, superficieUltimoYear, numeroFamiliares, trabajadoresPermanentes, trabajadoresTemporales,direccionProduccion ) {
       var toSend= {"Aplicacion":"negocios", "UserId":UserId,"Nombre":Nombre,"SegundoNombre":SegundoNombre,"Apellido":Apellido,"Correo":Correo,"DocIdentidad":DocIdentidad,"NivelEducativo":NivelEducativo,"IdMunicipio":IdMunicipio,"Direccion":Direccion,"Genero":Genero,"NumeroCelular":NumeroCelular,"LugarDeProduccion":idLugarDeProduccion,"PerteneceAsociacion":PerteneceAsociacion,"Asociacion":Asociacion,"NitAsociacion":NitAsociacion,"IdActividadAgricola":IdActividadAgricola,"IdCategoriaProducto":IdCategoriaProducto,"Superficie":superfice,"ProduccionPrincipal":productionPricipal,"LugarDeProduccionName":municipioProduccion, "SuperficieUltimoYear":superficieUltimoYear, "NumeroFamiliares":numeroFamiliares, "TrabajadoresPermanentes":trabajadoresPermanentes, "TrabajadoresTemporales":trabajadoresTemporales, "DireccionProduccion":direccionProduccion };
      console.log("JSON TO SEND : "+angular.toJson(toSend))  
       var promise  = $http({
           method : 'POST',
           url : URL+'ActualizarPerfil',
           data: toSend,
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('crearCompra', function($http, $log) {
    this.create = function(Cantidad,Descripcion,Direccion,FechaEntrega,UnidadMedida,IdCiudad,PresentacionCultivo,IdUsuario,Variedad,Estado,Republicar,Bonificacion,Latitud,Longitud) {
           var toSend={ "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud}
           console.log("JSON TO SEND : "+angular.toJson(toSend))         
           var promise  = $http({
           method : 'POST',
           url : URL+'CrearCompra',
           data: { "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('editarCompra', function($http, $log) {
    this.update = function(Cantidad,Descripcion,Direccion,FechaEntrega,UnidadMedida,IdCiudad,PresentacionCultivo,IdUsuario,Variedad,Estado,Republicar,Bonificacion,Latitud,Longitud) {
           var toSend={ "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud}
           console.log("JSON TO SEND : "+angular.toJson(toSend))         
           var promise  = $http({
           method : 'POST',
           url : URL+'EditarVenta',
           data: { "Cantidad":Cantidad,"Descripcion":Descripcion,"Direccion":Direccion,"FechaEntrega":FechaEntrega,"UnidadDeMedida":UnidadMedida,"IdCiudad":IdCiudad,"Presentacion":PresentacionCultivo,"Token":IdUsuario,"Variedad":Variedad,
           "Estado":Estado,"Republicar":Republicar,"Bonificacion":Bonificacion,"Latitud":Latitud,"Longitud":Longitud},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('removeOferta', function($http, $log) {
    this.remove = function(Id,token,state) {
        var toSend={ "Id":Id,"Token":token,"Estado":state}
           console.log("JSON TO SEND : "+angular.toJson(toSend))  
       var promise  = $http({
           method : 'POST',
           url : URL+'QuitarNegocio',
           data: { "Id":Id,"Token":token,"Estado":state},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('crearVenta', function($http, $log) {
       this.createventa = function(Cantidad,PrecioUnitario,VigenciaDesde,VigenciaHasta,Descripcion,FechaCreacion,FechaActualizacion,Republicar,Latitud,Longitud,Token,Variedad,Estado,UnidadMedida,Presentacion,Municipio) { 
       var toSend={ "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio}
       console.log("JSON TO SEND : "+angular.toJson(toSend))     
       var promise  = $http({
           method : 'POST',
           url : URL+'AgregarVenta',
           data: { "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('editarVenta', function($http, $log) {
       this.editar = function(Cantidad,PrecioUnitario,VigenciaDesde,VigenciaHasta,Descripcion,FechaCreacion,FechaActualizacion,Republicar,Latitud,Longitud,Token,Variedad,Estado,UnidadMedida,Presentacion,Municipio) { 
       var toSend={ "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio}
       console.log("JSON TO SEND : "+angular.toJson(toSend))     
       var promise  = $http({
           method : 'POST',
           url : URL+'EditarVenta',
           data: { "Cantidad":Cantidad,"PrecioUnitario":PrecioUnitario,"VigenciaDesde":VigenciaDesde,"VigenciaHasta":VigenciaHasta,"Descripcion":Descripcion,"FechaCreacion":FechaCreacion,"FechaActualizacion":FechaActualizacion,"Republicar":Republicar,"Latitud":Latitud,"Longitud":Longitud,"Token":Token,"Variedad":Variedad,
            "Estado":Estado,"UnidadMedida":UnidadMedida,"Presentacion":Presentacion,"IdCiudad":Municipio},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerVariedades', function($http, $log) {
    this.get = function(CategoryId,query) {
      $('#variedadlupa').hide();
      $('#varloader').show();

       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerVariedades',
           data: { "CategoryId":CategoryId,"Query":query},
           headers: headers,
       }).success(function(data, status, headers, config) {
            $('#varloader').hide();
            $('#variedadlupa').show();
          
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
            $('#varloader').hide();
            $('#variedadlupa').show();
       }); 
       return promise; 
    };
})
.service('saveFavorito', function($http, $log) {
    this.save = function(Id,token,estate) {
       var toSend={ "Id":Id,"Token":token,"Estado":estate}
       console.log("JSON TO SEND : "+angular.toJson(toSend))
       var promise  = $http({
           method : 'POST',
           url : URL+'AgregarFavoritoNegocios',
           data: { "Id":Id,"Token":token,"Estado":estate},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('removeFavorito', function($http, $log) {
    this.remove = function(Id,token,state) {
       var promise  = $http({
           method : 'POST',
           url : URL+'QuitarFavoritoNegocios',
           data: { "Id":Id,"Token":token,"Estado":state},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('getFavoritos', function($http, $log) {
    this.get = function(token) {
       console.log("JSON PETICION FavoritosCompra: "+angular.toJson({ "Token":token}))
       var promise  = $http({
           method : 'POST',
           url : URL+'FavoritosCompra',
           data: { "Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO FavoritosCompra: "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('getFavoritosVenta', function($http, $log) {
    this.get = function(token) {
      console.log("JSON PETICION FavoritosCompra: "+angular.toJson({ "Token":token}))
       var promise  = $http({
           method : 'POST',
           url : URL+'FavoritosVenta',
           data: {"Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO FavoritosVenta: "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('publicacionesVenta', function($http, $log) {
    this.get = function(token) {
       var promise  = $http({
           method : 'POST',
           url : URL+'MisPublicacionesVenta',
           data: { "Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('publicacionesCompra', function($http, $log) {
    this.get = function(token) {
       var promise  = $http({
           method : 'POST',
           url : URL+'MisPublicacionesCompra',
           data: { "Token":token},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('OlvidoClave', function($http, $log) {
    this.forgot = function(correo) {
       var promise  = $http({
           method : 'POST',
           url : URL+'RecoveryPass',
           data: { "Correo":correo},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('obtenerPerfil', function($http, $log) {
    this.get = function(id) {
      console.log("obtenerPerfil : "+id)
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerPerfil',
           data: { "Token":id},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetDetail', function($http, $log) {
    this.getBuyer = function(idCompra) { 
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerCompra',
           data: { "Id":idCompra},
           headers: headers,
       }).success(function(data, status, headers, config) {
           detail = data.Data;
           return detail;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetCityDefault', function($http, $log) {
    this.get = function(lat,lon) { 
       var promise  = $http({
           method : 'GET',
           url : URLGeo+'AgroInsumo/GetLocation?Long='+lon+'&Lat='+lat,
           headers: headers,
       }).success(function(data, status, headers, config) {
           detail = data.Data;
           return detail;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetMunicipios', function($http, $log) {
    this.getMunis = function(query) {
      console.log("<Services> GetMunicipios"+query);
		$('#municipiolupa').hide();
    $('#municipiolupaR').hide();
    $('#munlupa').hide();
		$('#munloader').show();
    $('#muniloader').show();
    $('#munloaderR').show();
       var promise  = $http({
           method : 'POST',
           url : URL+'ObtenerMunicipios',
           data: { "query":query},
           headers: headers,
       }).success(function(data, status, headers, config) {
		   $('#municipiolupa').show();
       $('#munlupa').show();
		   $('#munloader').hide();
       $('#muniloader').hide();
       $('#municipiolupaR').show();
       $('#munloaderR').hide();
       
    
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
		   $('#municipiolupa').show();
       $('#munlupa').show();
			 $('#muniloader').hide();
       $('#munloader').hide();
       $('#municipiolupaR').show();
       $('#munloaderR').hide();

		//alert('Ocurrió un error al realizar la busqueda');
       }); 
	   
       return promise; 
    };
})
.service('CambiarClave', function($http, $log) {
    this.update = function(token,actual,nueva,confirmacion) {
      var toSend={ "UserId":token,"Nueva":nueva,"Confirmacion":confirmacion}
       console.log("JSON TO SEND : "+angular.toJson(toSend))
       var promise  = $http({
           method : 'POST',
           url : URL+'CambiarContrasena',
           data: { "UserId":token,"Actual":actual,"Nueva":nueva,"Confirmacion":confirmacion},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
})
.service('GetLocation',function($http, $log)
{
  this.getLocation = function(lat, log){
       var toSend={"Lat":lat,"Long":log}
       console.log("JSON TO SEND : "+angular.toJson(toSend))
      var promise  = $http({
           method : 'POST',
           url : URL+'GetLocation',
           data: toSend,
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
      return promise; 
  };

});