
/*VARIABLES Y FUNCIONES USADAS EN LOS CONTROLADORES*/
var compradores = [];
var ventas = [];
var compradoresbuf = [];
var ventasbuf = [];
var precios=[];
var preciosbuf=[];
var misventas=[];
var misCompras=[];
var misfavoritoscompra=[];
var misfavoritosventa=[];
var municipios = [{}];
var variedades = [{}];
var presentaciones = [{}];
var myCompra = {};
var myVenta = {};
var nameX="";
var detailFrom = {};
var detailFromPrecio = {};
var idBuy=0;
//var existUser = true;
var resultUser=false;
var textButton = "Crear una cuenta"
var municipio=0;
var desde=0;
var loadingDataCompradores=false;
var desdeVenta=0;
var loadingDataVendedores=false;
var desdePrecio=0;
var cont=0;
var stateTab=true
var stateTabFav=false
var fromFav=false
var tokenBuffer=""
var idDepartamento=0;
var latitude=0;
var longitude=0;

compradores = compradores.sort(function(a, b) {

  var airlineA = a.name.toLowerCase();
  var airlineB = b.name.toLowerCase();

  if(airlineA > airlineB) return 1;
  if(airlineA < airlineB) return -1;
  return 0;
});
angular.module('starter')
.controller('BuyCtrl', function($scope,$ionicSlideBoxDelegate,$ionicScrollDelegate,$ionicHistory,$state,$ionicPopup,$localstorage,$filter,$ionicLoading,InfoService,obtenerListasPerfilCategorias,MunicipiosDataService,ConnectivityMonitor,cordovaGeolocationService,VariedadesDataService,crearCompra,PresentacionesDataService) {

    $scope.isMunicipio=false;
    $scope.isVariedades=false;
    $scope.isPresentaciones=false;
    $scope.count=0;

    var municipioLocal=0;
    var departamentoLocal=0;
    var cate=0
    var varId=0;
    var preId=0;

    $scope.categorias = [];
    $scope.presentaciones = [];
    $scope.municipiosdataregister = { "municipiosregister" : [] };
    $scope.muni={ title: ''};

    $scope.presentacionesbuf = { "presentaciones" : [] };
    $scope.present={ title: ''};
    $scope.inicialdate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.vararray = { "variedades" : [] };
    $scope.variedad={ title: ''};
    $scope.date = new Date();

     $scope.userModel = {
       product : '',
       variety: '',
       cant:0,
       suge:'',
       date:$filter('date')(new Date(), 'dd/MM/yyyy'),
       located:'',
       finalcoment:'',
       bonus:0,
       category:'',
       ubidadmedida:1,
       republicar:0
    }
   $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
    obtenerListasPerfilCategorias.register().success(function(responsec){
                    var dataresc=responsec;
                     $ionicLoading.hide();
                    if(dataresc.Estado==1) {
                       console.log("SET CATEGORY ARRAY");
                       $scope.categorias =  dataresc.Data.Categorias;
                       $scope.presentaciones=dataresc.Data.Presentaciones;
                        $scope.uMedida=dataresc.Data.Medidas;
                       
                       presentaciones=$scope.presentaciones;
                       console.log("SET PRESENTATION ARRAY");
                    }
                    else
                         {
                                $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No hay datos por mostrar.'
                                }).then(function() {
                                  
                                }); 

                         }

   }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });

   $scope.searchMunicipios = function(muni) {
   $scope.isMunicipio=true;
   console.log("ENTRA : "+muni);
   MunicipiosDataService.searchMunicipios(muni).then(

        function(matches) {

             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.municipiosdataregister.municipiosregister = matches;
             //$scope.scrollTop()
        }
      )
    }

   $scope.searchPresentaciones = function(muni) {
   $scope.isPresentaciones=true;
   console.log("ENTRA : "+muni);
   if(muni!=null){
   PresentacionesDataService.searchPresentaciones(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.presentacionesbuf.presentaciones = matches;
             //$scope.scrollTop()
        }
      )
    }
    else
    {
      $scope.isPresentaciones=false;
    }
  }

    $scope.searchVariedad = function(muni) {
      if(cate!=0)
      {
           $scope.isVariedades=true;
           console.log("ENTRA : "+muni);
           VariedadesDataService.searchVariedades(cate,muni).then(
           function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.vararray.variedades = matches;
             //$scope.scrollTop()
           })
      }
      else
      {
        if(cont==0)
        {
          cont=1;
          var myPopup = $ionicPopup.show({
          title: 'Atencion',
          template: 'Debe seleccionar una categoria de producto',
          scope: $scope,
          buttons: [
          {
              text: '<b>Ok</b>',
              type: 'button-positive',
              onTap: function(e) {
                  cont=0;
               }
             }
            ]
          });
        }

      }
    }
   
    $scope.seleccionar = function(item){
      console.log("SELECTED P: "+angular.toJson(item));
      this.muni.title=item.Nombre+" "+item.Departamento;
      municipioLocal=item.Id;
      departamentoLocal=item.IdDepartamento;
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }

     $scope.seleccionarVar = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.variedad.title=item.Nombre;
         varId=item.Id;
         console.log("SELECTED ID : "+varId);
         $scope.isVariedades  = false;
    }

     $scope.seleccionarPre = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.present.title=item.Nombre;
         preId=item.Id;
         console.log("SELECTED ID : "+preId);
         $scope.isPresentaciones  = false;
    }

    $scope.seleccionarCatego = function(id){
       console.log("SELECTED ID CATE: "+id);
       angular.forEach($scope.categorias, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);
                     }
       });
    }
    $scope.seleccionarMedida = function(id){
       console.log("SELECTED ID medida: "+id);
       angular.forEach($scope.uMedida, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        medidaId=c.Id
                        console.log("SELECT ID!!!: "+medidaId);
                     }
       });
    }

    var dataUs=$localstorage.get('tokenUser')
    console.log("TOKEN  : "+angular.toJson(dataUs))
    $scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
   
    $scope.actionButtonPublicar = function(theForm,form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      if(true)//if(onLine)
      {
         if(cate!=0 && varId!=0 && municipioLocal!=0 && preId!=0) {
           console.log("ID USER : "+dataUs)
           $scope.date=$filter('date')($scope.userModel.date, 'dd/MM/yyyy');
           console.log("NEW DATE TIME : "+$scope.date);

           $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
           });

            if($scope.currentPosition!==undefined){
               cLatitud = $scope.currentPosition.coords.latitude.toString();
               cLongitud = $scope.currentPosition.coords.longitude.toString();
            }
            else{
              console.log("********-----------------------------------");
              cLatitud=0.0;
              cLongitud=0.0;
            }

           crearCompra.create($scope.userModel.cant,$scope.userModel.finalcoment,
            $scope.muni.title,$scope.date ,$scope.userModel.unidad,
            municipioLocal,preId,dataUs,varId,1,$scope.userModel.republicar,$scope.userModel.bonus,cLatitud,cLongitud).success(function(response){
               var datares=response;
               if(datares.Estado== 1) {
                 //$state.go('app.login-result') 
                 $ionicLoading.hide()
                  var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Su publicacion sera aprobada muy pronto',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {

                              $ionicHistory.nextViewOptions({
                                disableAnimate: true,
                                disableBack: true
                              });
                              $state.go('app.homeclass') 
                         }
                        }
                       ]
                   });
               }
               else{
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                });
              }
            }).error(function(data,status, headers, config) {
                          console.log('Error de conexión, por favor intente nuevamente');
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                      $ionicHistory.nextViewOptions({
                                        disableAnimate: true,
                                        disableBack: true
                                      });
                                      $state.go('app.homeclass');
                                });
                               });
          }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            });
        }
      }
      $ionicScrollDelegate.scrollTop()
      $scope.stateRegistre = true
      $scope.stateSugesstion = false
      $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
      $scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
        
    }

    $scope.getCurrentPosition = function () {
            cordovaGeolocationService.getCurrentPosition(successHandler);
    };
    $scope.startWatchingPosition = function () {
            $scope.watchId = cordovaGeolocationService.watchPosition(successHandler);
    };
    $scope.stopWatchingPosition = function () {
            cordovaGeolocationService.clearWatch($scope.watchId);
            $scope.watchId = null;
            $scope.currentPosition = null;
    };
    // Handlers
    var successHandler = function (position) {
            $scope.currentPosition = position;
            //alert("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
            //console.log("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
    };
     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }
    $scope.nextSlide = function() {
       $ionicSlideBoxDelegate.next();
       console.log("fucckkk xxx: "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    }

    $scope.previusSlide = function() {
       $ionicSlideBoxDelegate.previous();
       console.log("fucckkk : "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    } 
     $scope.slideHasChanged = function($index){
          $scope.count=$index;
          console.log("fucckkk yyy: "+$scope.count);
          $ionicScrollDelegate.scrollTop();
          $scope.slideIndex = $index;
      
    };
   

    $scope.getCurrentPosition();

})
.controller('accessCtrl', function($scope, $ionicScrollDelegate,$ionicHistory,$state,cordovaGeolocationService){

    $scope.$on('$ionicView.loaded', function() {// CHANGE HERE
             ionic.Platform.ready( function() {
              if(navigator && navigator.splashscreen) navigator.splashscreen.hide();
         });
    });
     $scope.getCurrentPosition = function () {
            cordovaGeolocationService.getCurrentPosition(successHandler);
    };
    $scope.startWatchingPosition = function () {
            $scope.watchId = cordovaGeolocationService.watchPosition(successHandler);
    };
    $scope.stopWatchingPosition = function () {
            cordovaGeolocationService.clearWatch($scope.watchId);
            $scope.watchId = null;
            $scope.currentPosition = null;
    };
    // Handlers
    var successHandler = function (position) {
            $scope.currentPosition = position;
            //alert("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
            //console.log("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
    };
    /*var onLineProfile=ConnectivityMonitor.checkConnection();
    console.log("IS ONLINE?? "+onLineProfile+"   vs   ");
    if(!onLineProfile){
          $ionicPopup.alert({
                          title: 'Atención',
                         template: 'En este momento no se encuentra conectado a Internet. Por favor verifique su conexión e intente de nuevo'
                     }).then;
     
    }
    else
    {
      //alert("here : "+onLineProfile+" vs    "+xxx);
    }*/
    $scope.getCurrentPosition();
})
.controller('verCompraCtrl', function($scope,$ionicSlideBoxDelegate,$ionicScrollDelegate,$ionicHistory,$state,$ionicPopup,$localstorage,$filter,$ionicLoading,InfoService,obtenerListasPerfilCategorias,MunicipiosDataService,ConnectivityMonitor,cordovaGeolocationService,VariedadesDataService,editarCompra,PresentacionesDataService, obtenerVariedades) {

    $scope.isMunicipio=false;
    $scope.isVariedades=false;
    $scope.isPresentaciones=false;
    $scope.republicar=false;
    var municipioLocal=0;
    var departamentoLocal=0;
    $scope.count=0;
    $scope.currentdate = $filter('date')(new Date(), 'dd/MM/yyyy');
    var cate=0
    var varId=0;
    var preId=0;

    $scope.categorias = [];
    $scope.presentaciones = [];
    $scope.municipiosdataregister = { "municipiosregister" : [] };
    $scope.muni={ title: ''};

    $scope.presentacionesbuf = { "presentaciones" : [] };
    $scope.present={ title: ''};

    $scope.vararray = { "variedades" : [] };
    $scope.variedad={ title: ''};
    $scope.date = new Date();
    $scope.dateCompare=$filter('date')($scope.date, 'dd/MM/yyyy')
    $scope.hasta = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.inicialdate = $filter('date')(new Date(), 'dd/MM/yyyy');

     $scope.userModel = {
       product : '',
       variety: '',
       cant:0,
       suge:'',
       date:$filter('date')(new Date(), 'dd/MM/yyyy'),
       located:'',
       finalcoment:'',
       bonus:0,
       category:'',
       ubidadmedida:0,
       republicar:0
    }

    console.log("data to set : "+angular.toJson(myCompra));
    $scope.userModel.product =myCompra.Producto
    $scope.userModel.cant =myCompra.Cantidad
    $scope.muni.title =myCompra.Direccion
    $scope.userModel.finalcoment=myCompra.Descripcion
    municipioLocal=myCompra.MunicipioId
    console.log("Fucking DATE : "+myCompra.FechaEntrega);
    var mydate=myCompra.FechaEntrega
    var dateSplit = mydate.split("/")
    var day=parseInt(dateSplit[0])
    console.log("DAY: "+day)
    var month=parseInt(dateSplit[1])-1
    console.log("MONTH: "+month)
    var year=parseInt(dateSplit[2])
    console.log("YEAR: "+year)
    $scope.userModel.date= new Date(year,month,day,0,0,0,0)//$filter('date')(myCompra.FechaEntrega, 'dd/MM/yyyy')
    console.log("Fucking DATE 2: "+$scope.userModel.date)
    $scope.hasta=$filter('date')($scope.userModel.date, 'dd/MM/yyyy')
    console.log("another data to compare : "+$scope.hasta+" VS : "+$scope.dateCompare);
    $scope.userModel.finalcoment=myCompra.Descripcion
    $scope.userModel.bonus=myCompra.Bonificacion
    $scope.variedad.title=myCompra.Variedad
    $scope.present.title=myCompra.PresentacionCultivo
    //varId=myCompra.Variedad // HACE FALTA EL NOMBRE DE LA VARIEDAD YA QUE SOLO SE TIENE EL ID
    presentacionCultivo=myCompra.PresentacionCultivo
    //municipioLocal=myCompra.IdCiudad
    //cate=myCompra.IdCategoria HACE FALTA TENER ESTA INFO
    obtenerListasPerfilCategorias.register().success(function(responsec){
        var dataresc=responsec;
        if(dataresc.Estado==1) {
           
           $scope.categorias =  dataresc.Data.Categorias;
           angular.forEach($scope.categorias, function(c, key) {
                 if(c.Id==cate){
                   console.log("ENTERING IdCategoriaProducto: "+c.Nombre);
                   $scope.userModel.category =myCompra.Nombre;
                 }
            });
           //console.log("SET CATEGORY ARRAY"+JSON.stringify(dataresc.Data.Presentaciones));
           $scope.presentaciones =  dataresc.Data.Presentaciones;
           angular.forEach($scope.presentaciones, function(c, key) {
                 if(c.Nombre==presentacionCultivo){
                   preId=c.Id;
                   console.log("ENTERING presentacion nombre: "+c.Nombre);
                   //$scope.present.title =myCompra.Nombre;
                 }
            });
        }else{
                           $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No hay datos por mostrar'
                                }).then(function() {
                                  
                                }); 
        }
     }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
   $scope.searchMunicipios = function(muni) {
   $scope.isMunicipio=true;
   console.log("ENTRA : "+muni);
   MunicipiosDataService.searchMunicipios(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.municipiosdataregister.municipiosregister = matches;
        }
      )
    }

   $scope.searchPresentaciones = function(muni) {
   $scope.isPresentaciones=true;
   console.log("ENTRA : "+muni);
   if(muni!=null){
   PresentacionesDataService.searchPresentaciones(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.presentacionesbuf.presentaciones = matches;
        }
      )
    }
    else
    {
      $scope.isPresentaciones=false;
    }
  }

    $scope.searchVariedad = function(muni) {
      if(cate!=0)
      {
           $scope.isVariedades=true;
           console.log("ENTRA : "+muni);
           VariedadesDataService.searchVariedades(cate,muni).then(
           function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.vararray.variedades = matches;
           })
      }
      else
      {
        if(cont==0)
        {
          cont=1;
          var myPopup = $ionicPopup.show({
          title: 'Atencion',
          template: 'Debe seleccionar una categoria de producto',
          scope: $scope,
          buttons: [
          {
              text: '<b>Ok</b>',
              type: 'button-positive',
              onTap: function(e) {
                  cont=0;
               }
             }
            ]
          });
        }

      }
    }
   
    $scope.seleccionar = function(item){
      console.log("SELECTED P: "+angular.toJson(item));
      this.muni.title=item.Nombre+" "+item.Departamento;
      municipioLocal=item.Id;
      departamentoLocal=item.IdDepartamento;
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }

     $scope.seleccionarVar = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.variedad.title=item.Nombre;
         varId=item.Id;
         console.log("SELECTED ID : "+varId);
         $scope.isVariedades  = false;
    }

     $scope.seleccionarPre = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.present.title=item.Nombre;
         preId=item.Id;
         console.log("SELECTED ID : "+preId);
         $scope.isPresentaciones  = false;
    }

    $scope.seleccionarCatego = function(id){
       console.log("SELECTED ID CATE: "+id);
       angular.forEach($scope.categorias, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);

                        obtenerVariedades.get(cate,myCompra.Variedad).success(function(response){
                          var datares=response;
                          console.log("idVariedad dto"+JSON.stringify(datares));
                          if (datares.Data.length==0){
                              $scope.variedad.title='';

                          }
                          else {
                            varId = datares.Data[0].Id;
                            console.log("idVariedad"+varId);
                          }
                          

                        })

                        ;
                     }
       });
    }

    var dataUs=$localstorage.get('tokenUser')
    console.log("TOKEN  : "+angular.toJson(dataUs))
    $scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
   
    $scope.actionButtonPublicar = function(theForm,form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      //if(onLine)
      //{
        console.log("cate "+cate);
         console.log("varId "+varId);
          console.log("municipioLocal "+municipioLocal);
           console.log("preId "+preId);
         if(cate!=0 && varId!=0 && municipioLocal!=0 && preId!=0) {
           console.log("ID USER : "+dataUs)
           $scope.date=$filter('date')($scope.userModel.date, 'dd/MM/yyyy');
           console.log("NEW DATE TIME : "+$scope.date);

            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            }); 
             if($scope.currentPosition!==undefined){
               cLatitud = $scope.currentPosition.coords.latitude.toString();
               cLongitud = $scope.currentPosition.coords.longitude.toString();
            }
            else{
              cLatitud=0.0;
              cLongitud=0.0;
            }
           editarCompra.update($scope.userModel.cant,$scope.userModel.finalcoment,
            $scope.muni.title,$scope.date ,$scope.userModel.ubidadmedida,
            municipioLocal,preId,dataUs,varId,1,$scope.userModel.republicar,$scope.userModel.bonus,cLatitud,cLongitud).success(function(response){
               var datares=response;
               console.log(datares.Estado);
               console.log(response.Texto);
               if(datares.Estado== 1) {
                 //$state.go('app.login-result') 
                  $ionicLoading.hide()
                  var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Su publicacion sera aprobada muy pronto',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {

                              $ionicHistory.nextViewOptions({
                                disableAnimate: true,
                                disableBack: true
                              });
                              $state.go('app.homeclass') 
                               }
                              //$scope.cleanStack()
                         }
                        
                       ]
                   });
               }
               else{
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                });
              }
            }).error(function(response , status , headers ,config ,statusText) {
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Error de conexion',
                  template: 'No fue posible conectar al servidor, intentelo mas tarde.'
                });
            });
          }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            });
        }
      //}
      $ionicScrollDelegate.scrollTop()
      $scope.stateRegistre = true
      $scope.stateSugesstion = false
      $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
      $scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
        
    }

    $scope.actionButtonRePublicar = function(theForm,form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      if(onLine)
      {
         if(cate!=0 && varId!=0 && municipioLocal!=0 && preId!=0) {
            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
           console.log("ID USER : "+dataUs)
           $scope.date=$filter('date')($scope.userModel.date, 'dd/MM/yyyy');
           console.log("NEW DATE TIME : "+$scope.date);
           $scope.userModel.republicar=true;
            if($scope.currentPosition!==undefined){
               cLatitud = $scope.currentPosition.coords.latitude.toString();
               cLongitud = $scope.currentPosition.coords.longitude.toString();
            }
            else{
              cLatitud=0.0;
              cLongitud=0.0;
            }
           editarCompra.update($scope.userModel.cant,$scope.userModel.finalcoment,
            $scope.muni.title,$scope.date ,$scope.userModel.ubidadmedida,
            municipioLocal,preId,dataUs,varId,1,$scope.userModel.republicar,$scope.userModel.bonus,cLatitud,cLongitud).success(function(response){
               var datares=response;
               if(datares.Estado== 1) {
                 //$state.go('app.login-result') 
                 $ionicLoading.hide()
                  var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Su publicacion sera aprobada muy pronto',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                              $scope.cleanStack()
                         }
                        }
                       ]
                   });
               }
               else{
                 $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.data.Texto
                });
              }
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
          }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            }).then;
        }
      }
    
      $ionicScrollDelegate.scrollTop()
      $scope.stateRegistre = true
      $scope.stateSugesstion = false
      $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
      $scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
        
    }

    $scope.getCurrentPosition = function () {
            cordovaGeolocationService.getCurrentPosition(successHandler);
    };
    $scope.startWatchingPosition = function () {
            $scope.watchId = cordovaGeolocationService.watchPosition(successHandler);
    };
    $scope.stopWatchingPosition = function () {
            cordovaGeolocationService.clearWatch($scope.watchId);
            $scope.watchId = null;
            $scope.currentPosition = null;
    };
    // Handlers
    var successHandler = function (position) {
            $scope.currentPosition = position;
            //alert("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
            //console.log("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
    };
     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }

     $scope.nextSlide = function() {
       $ionicSlideBoxDelegate.next();
       console.log("fucckkk xxx: "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    }

    $scope.previusSlide = function() {
       $ionicSlideBoxDelegate.previous();
       console.log("fucckkk : "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    } 
     $scope.slideHasChanged = function($index){
          $scope.count=$index;
          console.log("fucckkk yyy: "+$scope.count);
          $ionicScrollDelegate.scrollTop();
          $scope.slideIndex = $index;
      
    };

    $scope.getCurrentPosition();

})
.controller('verVentaCtrl', function($scope,$ionicSlideBoxDelegate,$ionicScrollDelegate,$ionicHistory,$state,$ionicPopup,$localstorage,$filter,$ionicLoading,InfoService,obtenerListasPerfilCategorias,MunicipiosDataService,ConnectivityMonitor,cordovaGeolocationService,VariedadesDataService,PresentacionesDataService,editarVenta) {

    $scope.isMunicipio=false;
    $scope.isVariedades=false;
    $scope.isPresentaciones=false;
    $scope.republicar=false;
    $scope.count=0;
    var municipioLocal=0;
    var departamentoLocal=0;
    $scope.currentdate = new Date();
    var cate=0
    var varId=0;
    var preId=0;

    $scope.categorias = [];
    $scope.presentaciones = [];
    $scope.municipiosdataregister = { "municipiosregister" : [] };
    $scope.muni={ title: ''};

    $scope.presentacionesbuf = { "presentaciones" : [] };
    $scope.present={ title: ''};
    $scope.hasta = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.vararray = { "variedades" : [] };
    $scope.variedad={ title: ''};
    $scope.date = new Date();
    $scope.inicialdate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.dateCompare=$filter('date')($scope.date, 'dd/MM/yyyy')

     $scope.userModel = {
       product : '',
       variety: '',
       cant:0,
       costuni:0,
       totalcost:0,
       date:new Date(),
       coment:'',
       finalcoment:'',
       category:'',
       VigenciaDesde:$filter('date')(new Date(), 'dd/MM/yyyy'),
       VigenciaHasta:$filter('date')(new Date(), 'dd/MM/yyyy'),
       FechaActualiza:$filter('date')(new Date(), 'dd/MM/yyyy'),
       FechaCreacion:$filter('date')(new Date(), 'dd/MM/yyyy'),
       republicar:0,
       estado:0,
       unidad:0,
       municipioId:0
    }

    console.log("data to set : "+angular.toJson(myVenta));
    $scope.userModel.product =myVenta.Producto
    $scope.userModel.cant =myVenta.Cantidad
    $scope.userModel.costuni =myVenta.PrecioUnitario
    $scope.muni.title =myVenta.Municipio
    $scope.userModel.finalcoment=myVenta.Descripcion
    var mydate=myVenta.VigenciaHasta
    var dateSplit = mydate.split("/")
    var day=parseInt(dateSplit[0])
    console.log("DAY: "+day)
    var month=parseInt(dateSplit[1])-1
    console.log("MONTH: "+month)
    var year=parseInt(dateSplit[2])
    console.log("YEAR: "+year)
    $scope.userModel.date=new Date(year,month,day,0,0,0,0)//myVenta.VigenciaHasta//$filter('date')(myCompra.FechaEntrega, 'dd/MM/yyyy')
    console.log("FUCKING DATE BLA : "+$scope.userModel.date);
    $scope.hasta=$filter('date')($scope.userModel.date, 'dd/MM/yyyy')//$scope.userModel.date
    console.log("another data to compare : "+$scope.hasta+" VS : "+$scope.dateCompare);
    $scope.variedad.title=myVenta.Variedad;
    $scope.present.title=myVenta.Presentacion;

    varId=myVenta.Variedad//HARIA FALTA ESTE CAMPO YA QUE NO TENGO EL NOMBRE SOLO EL ID
    preId=myVenta.Presentacion
    municipioLocal=myVenta.IdCiudad
    //cate=myVenta.Categoria


     obtenerListasPerfilCategorias.register().success(function(responsec){
        var dataresc=responsec;
        if(dataresc.Estado==1) {
           console.log("SET CATEGORY ARRAY");
           $scope.categorias =  dataresc.Data.Categorias;
           angular.forEach($scope.categorias, function(c, key) {
                 if(c.Id==cate){
                   console.log("ENTERING IdCategoriaProducto: "+c.Nombre);
                   $scope.userModel.category =myVenta.Nombre;
                 }
            });

           $scope.presentaciones =  dataresc.Data.Presentaciones;
           angular.forEach($scope.presentaciones, function(c, key) {
                 if(c.Id==preId){
                   console.log("ENTERING IdCategoriaProducto: "+c.Nombre);
                   $scope.present.title =myVenta.Nombre;
                 }
            });
        }else{
                            $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No hay datos por mostrar.'
                                }).then(function() {
                                  
                                }); 

        }
     }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });

   $scope.searchMunicipios = function(muni) {
   $scope.isMunicipio=true;
   console.log("ENTRA : "+muni);
   MunicipiosDataService.searchMunicipios(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.municipiosdataregister.municipiosregister = matches;
             //$scope.scrollTop()
        }
      )
    }

   $scope.searchPresentaciones = function(muni) {
   $scope.isPresentaciones=true;
   console.log("ENTRA : "+muni);
   if(muni!=null){
   PresentacionesDataService.searchPresentaciones(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.presentacionesbuf.presentaciones = matches;
             //$scope.scrollTop()
        }
      )
    }
    else
    {
      $scope.isPresentaciones=false;
    }
  }

    $scope.searchVariedad = function(muni) {
      if(cate!=0)
      {
           $scope.isVariedades=true;
           console.log("ENTRA : "+muni);
           VariedadesDataService.searchVariedades(cate,muni).then(
           function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.vararray.variedades = matches;
             //$scope.scrollTop()
           })
      }
      else
      {
        if(cont==0)
        {
          cont=1;
          var myPopup = $ionicPopup.show({
          title: 'Atencion',
          template: 'Debe seleccionar una categoria de producto',
          scope: $scope,
          buttons: [
          {
              text: '<b>Ok</b>',
              type: 'button-positive',
              onTap: function(e) {
                  cont=0;
               }
             }
            ]
          });
        }

      }
    }
   
    $scope.seleccionar = function(item){
      console.log("SELECTED P: "+angular.toJson(item));
      this.muni.title=item.Nombre+" "+item.Departamento;
      municipioLocal=item.Id;
      departamentoLocal=item.IdDepartamento;
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }

     $scope.seleccionarVar = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.variedad.title=item.Nombre;
         varId=item.Id;
         console.log("SELECTED ID : "+varId);
         $scope.isVariedades  = false;
    }

     $scope.seleccionarPre = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.present.title=item.Nombre;
         preId=item.Id;
         console.log("SELECTED ID : "+preId);
         $scope.isPresentaciones  = false;
    }

    $scope.seleccionarCatego = function(id){
       console.log("SELECTED ID CATE: "+id);
       angular.forEach($scope.categorias, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);
                     }
       });
    }

    var dataUs=$localstorage.get('tokenUser')
    console.log("TOKEN  : "+angular.toJson(dataUs))
    $scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
   
    $scope.actionButtonPublicar = function(theForm,form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      if(onLine)
      {
         if(cate!=0 && varId!=0 && municipioLocal!=0 && preId!=0) {
           console.log("ID USER : "+dataUs)
            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            }); 
           $scope.date=$filter('date')($scope.userModel.date, 'dd/MM/yyyy');
           console.log("NEW DATE TIME : "+$scope.date);
            if($scope.currentPosition!==undefined){
               cLatitud = $scope.currentPosition.coords.latitude.toString();
               cLongitud = $scope.currentPosition.coords.longitude.toString();
            }
            else{
              cLatitud=0.0;
              cLongitud=0.0;
            }

           editarVenta.editar($scope.userModel.cant,$scope.userModel.costuni,
            $scope.userModel.VigenciaDesde,$scope.date ,$scope.userModel.finalcoment,$scope.userModel.FechaCreacion,
            $scope.userModel.FechaActualiza,$scope.userModel.republicar,cLatitud,
            cLongitud,dataUs,varId,$scope.userModel.estado,$scope.userModel.unidad,preId,
            municipioLocal).success(function(response){
               var datares=response;
               if(datares.Estado== 1) {
                 //$state.go('app.login-result') 
                  $ionicLoading.hide()
                  var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Su publicacion sera aprobada muy pronto',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                              //$scope.cleanStack()

                                    $ionicHistory.nextViewOptions({
                                        disableAnimate: true,
                                        disableBack: true
                                      });
                                      $state.go('app.homeclass');
                         }
                        }
                       ]
                   });
               }
               else{
                  $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                });
              }
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
          }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            }).then;
        }
      }
      $ionicScrollDelegate.scrollTop()
      $scope.stateRegistre = true
      $scope.stateSugesstion = false
      $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
      $scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
        
    }

    $scope.actionButtonRePublicar = function(theForm,form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      if(onLine)
      {
         if(cate!=0 && varId!=0 && municipioLocal!=0 && preId!=0) {
            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            }); 
           console.log("ID USER : "+dataUs)
           $scope.userModel.republicar=true;
           $scope.date=$filter('date')($scope.userModel.date, 'dd/MM/yyyy');
           console.log("NEW DATE TIME : "+$scope.date);

            if($scope.currentPosition!==undefined){
               cLatitud = $scope.currentPosition.coords.latitude.toString();
               cLongitud = $scope.currentPosition.coords.longitude.toString();
            }
            else{
              cLatitud=0.0;
              cLongitud=0.0;
            }

           editarVenta.editar($scope.userModel.cant,$scope.userModel.costuni,
            $scope.userModel.VigenciaDesde,$scope.date ,$scope.userModel.finalcoment,$scope.userModel.FechaCreacion,
            $scope.userModel.FechaActualiza,$scope.userModel.republicar,cLatitud,cLongitud,
            dataUs,varId,$scope.userModel.estado,$scope.userModel.unidad,preId,
            municipioLocal).success(function(response){
               var datares=response;
               if(datares.Estado== 1) {
                 //$state.go('app.login-result') 
                  $ionicLoading.hide()
                  var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Su publicacion sera aprobada muy pronto',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                              $scope.cleanStack()
                         }
                        }
                       ]
                   });
               }
               else{
                 $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                }).then;
              }
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
          }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            }).then;
        }
      }
      
      $ionicScrollDelegate.scrollTop()
      $scope.stateRegistre = true
      $scope.stateSugesstion = false
      $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
      $scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
        
    }

    $scope.getCurrentPosition = function () {
            cordovaGeolocationService.getCurrentPosition(successHandler);
    };
    $scope.startWatchingPosition = function () {
            $scope.watchId = cordovaGeolocationService.watchPosition(successHandler);
    };
    $scope.stopWatchingPosition = function () {
            cordovaGeolocationService.clearWatch($scope.watchId);
            $scope.watchId = null;
            $scope.currentPosition = null;
    };
    // Handlers
    var successHandler = function (position) {
            $scope.currentPosition = position;
            //alert("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
            //console.log("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
    };
     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }
    $scope.nextSlide = function() {
       $ionicSlideBoxDelegate.next();
       console.log("fucckkk xxx: "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    }

    $scope.previusSlide = function() {
       $ionicSlideBoxDelegate.previous();
       console.log("fucckkk : "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    } 
     $scope.slideHasChanged = function($index){
          $scope.count=$index;
          console.log("fucckkk yyy: "+$scope.count);
          $ionicScrollDelegate.scrollTop();
          $scope.slideIndex = $index;
      
    };

    $scope.getCurrentPosition();

})
.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})
.controller('DetailPrecioCtrl',function($scope,$filter,$localstorage,$ionicScrollDelegate,$ionicPopup,$ionicHistory,$cordovaSocialSharing,$ionicLoading,PreciosSipsa,InfoService ,PreciosFinca) {
  $scope.detail ={};
  $scope.detail=detailFromPrecio;
  var preId=0
  $scope.preInfo=""
  $scope.isSipsa=false;

    $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; "
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "

    var arrayDataT = InfoService.getDataT()
    var arrayDataP = InfoService.getDataP()
    var arrayDataN = InfoService.getDataN()
    var arrayDataAll = InfoService.getDataAll()

    $scope.sipsa = { "precios" : [] };
    $scope.sipsafinca = { "preciosfinca" : [] };
     $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Cargando precios....'
            });
    var nameSplit = nameX.split(" ");
    console.log("is true "+nameSplit[0]);
    $scope.tituloDetalle = $scope.detail.Producto;

    PreciosSipsa.getSipsa(nameSplit[0],$scope.detail.Producto, $scope.detail.CodProducto).success(function(responsecp){
//console.log("PreciosSipsa "+JSON.stringify(responsecp));

                   var datarescp=responsecp;
                   if(datarescp.Data.length>0){
                        angular.forEach(datarescp.Data, function(c, key) {
                              c.Fecha=$filter('date')(c.Fecha, 'dd/MM/yyyy')
                       });
                       $scope.sipsa.precios = datarescp.Data;
                       productoFinca= responsecp.Data[0].NombreProducto;
                       codProducto= responsecp.Data[0].CodProducto;

                       console.log(municipio+"////"+codProducto+"////"+productoFinca);
                       PreciosFinca.getSipsa(municipio, productoFinca,codProducto).success(function(responsec){
                            
                           var dataresc=responsec;
                           angular.forEach(datarescp.Data, function(c, key) {
                                  c.Fecha=$filter('date')(c.Fecha, 'dd/MM/yyyy')
                           });
                            console.log("PreciosFinca "+JSON.stringify(responsecp));
                           $ionicLoading.hide();
                              if(dataresc.Estado==1) {
                                  $ionicLoading.hide();
                                  $scope.sipsafinca.preciosfinca=dataresc.Data;
                                  
                              }
                              else
                              {
                                $ionicLoading.hide();

                              }
                       }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
                   }
                   else {
                    $ionicLoading.hide();
                      $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No se obtuvieron datos'
                                }).then(function() {
                                  $ionicHistory.goBack();
                                });
                   }
    }).error(function(data,status, headers, config) {
                          console.log('Error de conexión, por favor intente nuevamente');
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  $ionicHistory.goBack();
                                });
                               });

    $scope.actionInfo = function(tag){

        $scope.arrayData = [] 
        if(tag == 1 ) {// YAAA     
            $scope.arrayData = []
            $scope.arrayData = arrayDataAll
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "   
            $scope.isSipsa=false;
        }
        if(tag == 2 ) {       
            $scope.arrayData = []
            $scope.arrayData = arrayDataT
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            $scope.isSipsa=true;
        }
        
        $ionicScrollDelegate.scrollTop();
    }

    $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }

    $scope.shareAnywhere = function() {
        console.log("ENTER TO SOCIAL SHARING")  
         $cordovaSocialSharing.share("Precio estimado para: "+detailFromPrecio.Producto+" es: "+detailFromPrecio.PrecioPromedio, "Agronegocios", "", "")
          .then(function(result) {
              console.log("success SOCIAL SHARING: "+ result) 
            }, function(err) {
              console.log("ERROR SOCIAL SHARING: "+ err) 
            });
       
    }

})
.controller('logTestCtrl', function($scope,obtenerPerfil,$ionicScrollDelegate,$state,$ionicHistory,$ionicPopup,$localstorage,$ionicLoading,InfoService,LogIn,RegistroLogin,OlvidoClave,ConnectivityMonitor,CambiarClave) {
    $scope.existUser = true;
    console.log("set in logTestCtrl: "+ $scope.existUser)
    $scope.textButton = "Crear una cuenta"
    $scope.typeInput = "password";
    $scope.logo=false;
    $scope.texto=false;
    $scope.formModel = {
       email : '',
       password: ''
    }// NEW CHANGE


    var dataUs=$localstorage.get('tokenUser')
    console.log("SETTING TOKEN "+dataUs)
    if(dataUs!=null)
       $scope.texto=true;
    var stateViewPassword = true
    $scope.login = function(valid) {
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      //if(onLine)
      //{
       if(valid) {
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
          console.log("ENTRA AL BOTON LOGIN")
          LogIn.logIn($scope.formModel.email,$scope.formModel.password).success(function(response){
             var datares=response;
               if(datares.Estado== 1) {
                console.log("ISAVED TOKEN "+datares.Token);
                $localstorage.set('tokenUser', datares.Token);
                $localstorage.set('userName', ' ');       
                tokenBuffer=datares.Token;
                obtenerPerfil.get(datares.Token).success(function(responsep){
                   console.log("obtenerPerfil "+JSON.stringify(responsep));
                   var dataresp=responsep;
                     if(dataresp.Estado== 1) {
                        resultUser=true;
                        $ionicLoading.hide()
                        $localstorage.set('userName',dataresp.Data.Nombre+" "+dataresp.Data.SegundoNombre+" "+dataresp.Data.Apellido+" "+dataresp.Data.SegundoApellido);
                        //$state.go('app.homeclass');
                        /*$state.go('app.homeclass', {}, {reload: false}).then(function(){
                            setTimeout(function() {
                             window.location.reload(true);
                         }, 0);
                        })*/
                        $ionicHistory.nextViewOptions({
                          disableAnimate: true,
                          disableBack: true
                        });
                        $state.go('app.homeclass');
                        //$scope.cleanStack();
                      }
                      else{
                         $ionicLoading.hide()

                         $ionicPopup.alert({
                            title: 'Resultado',
                            template: response.Texto
                          }).then(function(res) {
                              $ionicHistory.nextViewOptions({
                                disableAnimate: true,
                                disableBack: true
                              });
                              $state.go('app.homeclass');

                             });

                      
                        }

                }).error(function(response , status , headers ,config ,statusText) {
                    $ionicLoading.hide()
                     $ionicPopup.alert({
                      title: 'Error de conexion',
                      template: 'No fue posible conectar al servidor, intentelo mas tarde.'
                    });
                });
                //InfoService.createToken(response.Token)
                //$state.go('app.homeclass');
                 //window.location.reload(true);
                 //history.go(0);
                 //window.location.href = window.location.href;
                 //$ionicHistory.go(0);
                  /* $state.go('app.homeclass', {}, {reload: false}).then(function(){
                       setTimeout(function() {
                        window.location.reload(true);
                    }, 500);
                 })*/
                 //$scope.cleanStack();
                 //route.reload();

             }
             else{
               $ionicLoading.hide()
               $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                }).then;
              }
       }).error(function(response , status , headers ,config ,statusText) {
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Error de conexion',
                  template: 'No fue posible conectar al servidor, intentelo mas tarde.'
                });
            });
      }
      else{
        $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus credenciales'
            }).then;
      }
    //}
   }; 
    $scope.existUserAction = function(){
        $ionicScrollDelegate.scrollTop();
        var dataUs=$localstorage.get('tokenUser')
        console.log("SETTING TOKEN "+dataUs)
        if(dataUs!=null)
          $scope.existUser =true
        else
          $scope.existUser =false  
        console.log("set in existUserAction: "+ $scope.existUser)
        $scope.textButton =  "Ingresar"
        $scope.logo=true;
    }
    $scope.goToHome= function() {
        $ionicHistory.goBack(-2);
         
    }
    $scope.showAlert = function(email) {
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      //if(onLine)
      //{
        console.log(email);
        if(!angular.equals($scope.formModel.email, ""))
        {
           $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
          OlvidoClave.forgot($scope.formModel.email).success(function(response){
            var datares=response;
            if(datares.Estado== 1) {
              $ionicLoading.hide()
               var alertPopup = $ionicPopup.alert({
                     title: 'Información',
                     template: response.Texto
               });
            alertPopup.then(function(res) {
            console.log('Thank you for not eating my delicious ice cream cone');
           });
        }
        else{
            $ionicLoading.hide()
           $ionicPopup.alert({
              title: 'Resultado',
               template: response.Texto
           });

          }
        }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
                
      }
      else
       {
           $ionicPopup.alert({
           title: 'Atencion',
           template: "Debe ingresar un correo electronico valido"
           });
       }
     //}
    };

$scope.showPopup = function() {
  $scope.data = {}
  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: '<input type="password" placeholder="Ingrese su nueva clave" ng-model="data.password"> <br>'+
              '<input type="password" placeholder="Re ingrese la clave" ng-model="data.repassword">',
    title: 'Restablesca su clave',
    subTitle: '',
    scope: $scope,
    buttons: [
      { text: 'Cancelar' },
      {
        text: '<b>Recuperar</b>',
        type: 'button-positive',
        onTap: function(e) {
          if (angular.equals($scope.data.password,$scope.data.repassword)) {
            var dataUs=$localstorage.get('tokenUser')
            console.log("SETTING TOKEN "+dataUs)
            CambiarClave.update(dataUs,$scope.formModel.password,$scope.data.repassword).success(function(response){
                       var datares=response;
                       if(datares.Estado== 1) {
                         $ionicPopup.alert({
                             title: 'Resultado',
                             template: response.Texto
                           });
                        }
                        else{
                          $ionicPopup.alert({
                             title: 'Resultado',
                             template: response.Texto
                           });
                        }
                  }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
            e.preventDefault();
          } else {
            $ionicPopup.alert({
                  title: 'Resultado',
                  template: "Las claves ingresadas no coinciden. Por favor rectifique e intente de nuevo"
                }).then;
            }
          }
         }
        ]
      });
    }

    $scope.onSubmit = function(valid){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      //if(onLine)
      //{
        console.log($scope.formModel.email + $scope.formModel.password);
        if (valid) {
              $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
             console.log("ENTRA AL BOTON "+$scope.logo)
             if(!$scope.logo)
             {
             RegistroLogin.register($scope.formModel.email,$scope.formModel.password).success(function(response){
               var datares=response;
               if(datares.Estado== 1){
                 LogIn.logIn($scope.formModel.email,$scope.formModel.password).success(function(response){
                       var datares=response;
                       if(datares.Estado== 1) {
                         $ionicLoading.hide()
                         $localstorage.set('tokenUser', datares.Token);
                         //InfoService.createToken(response.Token)
                         $state.go('app.login-result');
                        }
                        else{
                          $ionicLoading.hide()
                          $ionicPopup.alert({
                             title: 'Resultado',
                             template: response.Texto
                           });
                        }
                  }).error(function(response , status , headers ,config ,statusText) {
                          $ionicLoading.hide()
                           $ionicPopup.alert({
                            title: 'Error de conexion',
                            template: 'No fue posible conectar al servidor, intentelo mas tarde.'
                          });
                      });
               }
               else{
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                });
              }
            }).error(function(response , status , headers ,config ,statusText) {
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Error de conexion',
                  template: 'No fue posible conectar al servidor, intentelo mas tarde.'
                });
            });
            }
            else
            {
              $ionicLoading.hide()
               $scope.logo=false;
               $scope.login(valid)
            }

        } else {
            alert("Verifique sus datos")
        }
      //}
    }
    $scope.viewPassword = function(){
        if (stateViewPassword) {
            //Ver
            stateViewPassword = false
            $scope.typeInput = "text"
        }else {
            stateViewPassword = true
            $scope.typeInput = "password"
        }
    }
     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }
    
})
.controller('misPublicacionesCtrl',['$scope','$filter','$ionicLoading','$ionicPopup', '$state','$ionicHistory','InfoService','$ionicScrollDelegate','$localstorage','publicacionesCompra','publicacionesVenta','removeOferta',  function($scope,$filter,$ionicLoading,$ionicPopup,$state,$ionicHistory,InfoService,$ionicScrollDelegate,$localstorage,publicacionesCompra,publicacionesVenta,removeOferta){ 
    $scope.arrayData = InfoService.getDataAll()
    $scope.data = { "search" : '' };
    $scope.tabSelect=true;
    $scope.data.misbuy = { "miscompradores" : [] };
    $scope.data.missold = { "misventas" : [] };
    $scope.isVenta=false;


    var arrayDataT = InfoService.getDataT()
    var arrayDataP = InfoService.getDataP()
    var arrayDataN = InfoService.getDataN()
    var arrayDataAll = InfoService.getDataAll()
    var customers = {}

    $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; "
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
    var dataUs=$localstorage.get('tokenUser')
    $scope.actionInfo = function(tag){

        $scope.arrayData = [] 
        if(tag == 1 ) {     
            $scope.arrayData = []
            $scope.arrayData = arrayDataAll
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            $scope.tabSelect=true;
            $scope.isVenta=false;
 
        }
        if(tag == 2 ) {       
            $scope.arrayData = []
            $scope.arrayData = arrayDataT
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            $scope.tabSelect=false;
            $scope.isVenta=true;
        }
      }

     $scope.goToDetail = function(item){
        if($scope.tabSelect){
          myCompra=item;
          $state.go('app.tabdetailclasscompra');
        }
        else{
          myVenta=item;
          $state.go('app.tabdetailclassventa');
        }
    }

     $scope.deleteOferta = function(id){
        var dataUs=$localstorage.get('tokenUser')  
        $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });  
        removeOferta.remove(id,dataUs,$scope.tabSelect).success(function(response){
            var datares=response;
            if(datares.Estado== 1) {
              $ionicLoading.hide()
            var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Publicacion eliminada',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                              $scope.cleanStack()
                         }
                        }
                       ]
                   });
              
            }
            else
            {
               $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: datares.Texto
                }).then;

            }

        }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
    }

     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }
    $scope.loadAllData = function(){
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Obteniendo mis publicaciones...'
            });
           publicacionesCompra.get(dataUs).success(function(response){
              angular.forEach(response.Data, function(c, key) {
                        c.FechaEntrega=$filter('date')(c.FechaEntrega, 'dd/MM/yyyy')
              });
              $scope.data.misbuy.miscompradores = response.Data;
              publicacionesVenta.get(dataUs).success(function(responseventa){
                 angular.forEach(responseventa.Data, function(c, key) {
                       c.VigenciaHasta=$filter('date')(c.VigenciaHasta, 'dd/MM/yyyy')
                       c.TotalCost=c.Cantidad*c.PrecioUnitario
                 });
                 $scope.data.missold.misventas = responseventa.Data;
                 $ionicLoading.hide()
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); }); 
          }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
    }    
    console.log("ENTERING FUCKING HERE..");
    $scope.loadAllData();
}])
.controller('menuHomeCtrl',  function($scope,$localstorage,$state,$ionicHistory,InfoService){ 
    console.log("ENTERING FUCKING HERE.. menuHomeCtrl");
    $scope.existUser =false;   
    $scope.userModel = InfoService.getDataUser();
    $scope.nameuser="";
    $scope.porcentual="20";
    var dataUs=$localstorage.get('tokenUser');
    if(dataUs!=null){
           console.log($scope.userModel);
           $scope.nameuser =$scope.userModel.name;
           console.log("Cambiar aca el porcentaje del perfil!!!!");
           $scope.porcentual="100";
           console.log("SETTING FUCKING TOKEN CRITICAL "+dataUs);

            $scope.width="100%";
   
           $scope.nameuser =$localstorage.get('userName');
           $scope.existUser =true;
    }
   
    $scope.reloadStateUser = function(){
        
        $scope.nameuser = $localstorage.get('userName');
        $scope.tok=$localstorage.get('tokenUser');
        dataUs=$scope.tok;
        console.log("RESULT  : "+$scope.tok+"   VS   "+$scope.nameuser);
        if($scope.tok!=null && $scope.nameuser!=null)
        {
           $scope.existUser =true;
           console.log("IS SETTING TRUE OKKK");
        }

        reloadData();
    }

    function reloadData(){  
  
        console.log("SET EXISTS USER IN reloadData menuHomeCtrl : "+$scope.existUser);
    }

    
     $scope.shouldLeftSideMenuBeEnabled = function (){
        return true;
    }

    $scope.onGesture = function(){
        reloadData();
    }
    $scope.goTo = function(tag){
        if (tag == 1){
            $state.go('app.user');
        }
        if (tag == 2){
            $state.go('app.favorite');
        }
        if (tag == 3){
            $state.go('app.login');
        } 
        if (tag == 4){
            $state.go('app.mispublicaciones');
        }          
    }

    $scope.closeSesion = function(){
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
        //InfoService.deleteDB()
        console.log(" REMOVING TOKEN "+dataUs);
        $localstorage.set('tokenUser',"");
        $localstorage.set('userName',"");
        $scope.existUser=false;
        $state.go('acceso');
    }

   $scope.cambiarContra = function(){
        $state.go('app.cambio-clave');
        
       
    }

   // $scope.loadMoreData();
  

}).controller('cambioClaveCtrl', function($scope,$localstorage,$state,$ionicHistory,$ionicLoading,$ionicPopup, CambiarClave){
    
    console.log("inicioCambioClavectr");
    $scope.cambiarClave = function(){
        $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            }); 
              console.log("funcion cambiar clave");
              console.log("passwordActual"+$scope.userModel.passwordActual);
              console.log("passwordNuevo"+$scope.userModel.passwordNuevo);
              console.log("confirmPasswordNuevo"+$scope.userModel.confirmPasswordNuevo);
                            claveActual=  $scope.userModel.passwordActual
                            claveNueva =  $scope.userModel.passwordNuevo
                            confirmClave= $scope.userModel.confirmPasswordNuevo
            if(claveActual!=undefined&&claveNueva!=undefined&&confirmClave!=undefined&&claveActual!=''&&claveNueva!=''&&confirmClave!='')
            {
                            if(claveNueva!=confirmClave){
                               $ionicLoading.hide();
                                    $ionicPopup.alert({
                                            title: 'Error confirmación clave',
                                            template: 'Por favor verifique la confirmación de la clave'
                                        }).then(function() {
                                          
                                        });
                            }
                            else if(claveNueva.length<6){
                               $ionicLoading.hide();
                                  $ionicPopup.alert({
                                            title: 'Atención',
                                            template: 'Por favor verifique la longitud de la clave nueva, esta debe ser mayor a 6 caracteres'
                                        }).then(function() {
                                          
                                        });
                            }
                            else{
                              var dataUs=$localstorage.get('tokenUser');
                              CambiarClave.update(dataUs,claveActual, claveNueva,confirmClave).success(function(response){
                                      var datares=response;
                                      $ionicLoading.hide()
                                      if(datares.Estado== 1) {
                                        $ionicLoading.hide();
                                      var myPopupExit = $ionicPopup.show({
                                               title: 'Atencion',
                                               template: datares.Texto,
                                                scope: $scope,
                                                buttons: [
                                                {
                                                   text: '<b>Ok</b>',
                                                   type: 'button-positive',
                                                   onTap: function(e) {

                                                    $scope.closeSesion();
                                                        /*$ionicHistory.nextViewOptions({
                                                          disableAnimate: true,
                                                          disableBack: true
                                                        });
                                                        
                                                          $ionicHistory.clearHistory();
                                                          $ionicHistory.clearCache();
                                                          //InfoService.deleteDB()
                                                          console.log(" REMOVING TOKEN "+dataUs);
                                                          $localstorage.set('tokenUser',"");
                                                          $localstorage.set('userName',"");
                                                          $scope.existUser=false;
                                                          $state.go('acceso');
                                                            */


                                                   }
                                                  }
                                                 ]
                                             });
                                        
                                      }
                                      else
                                      {
                                         $ionicLoading.hide()
                                           $ionicPopup.alert({
                                            title: 'Resultado',
                                            template: datares.Texto
                                          }).then;

                                      }

                }).error(function(data,status, headers, config) {
                                       $ionicLoading.hide();
                                       $ionicPopup.alert({
                                            title: 'Atención',
                                            template: 'Error de conexión, por favor intente nuevamente.'
                                        }).then(function() {
                                          
                                        }); });




                            }
              }else{
                  $ionicLoading.hide();
                  $ionicPopup.alert({
                                            title: 'Error',
                                            template: 'Uno de los campos se encuentra vacio'
                                        }).then(function() {
                                          
                                        });
              }
                                      
            }




})

/*CONSTANTES PARA EL GEO REFERENCIADO*/
.constant('cordovaGeolocationConstants', {
    apiVersion: '1.0.0',
    cordovaVersion: '>=3.4.0'
})
/*DIRECTIVAS USADAS EN LA APLICACION*/
.directive('menuRegistre', function () {
  return {
    restrict: 'E',
    cache:false,
    templateUrl: 'views/directives/registre.html'
  };
})
.directive('menuSugesstion', function () {
  return {
    restrict: 'E',
    templateUrl: 'views/directives/sugesstion.html'
  };
})
/*FACTORIAS UTILIZADAS EN LA APP*/
/**
 * Main service relying on Apache Cordova Geolocation Plugin.
 */
 .factory('ConnectivityMonitor', function($rootScope,$ionicPopup){
  return {
    checkConnection : function() {
        /*if(navigator && navigator.connection && navigator.connection.type === 'none') {
            return false;
        }
        return true;*/
         var ret=false;
         if(window.Connection) {
              if(navigator.connection.type == Connection.NONE) {
                //ENTRA AQUI SI NO TIENE NINGUNA
                ret=false;
                 $ionicPopup.alert({//remplazo confirm por alert
                      title: 'Problemas de Conexión',
                      content: 'Disculpe, Por favor verifique su conexión a Internet.'
                 })
                 .then(function(result) {
                      /*if(!result) {
                       navigator.app.exitApp();
                      }*/
                 });
              }
              else
              {
                //red movil y wifi. Entra aqui si tiene alguna conexion
                ret=true;
              }
         }
         return ret;
    }
  }
})
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      console.log("SET KEY: "+key+" VALUE :"+value);
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])
.factory('cordovaGeolocationService', ['$rootScope', '$log', '$ionicPopup','cordovaGeolocationConstants', function ($rootScope, $log,$ionicPopup, cordovaGeolocationConstants) {
    return {
        /**
         * Return the current API version.
         */
        apiVersion: function () {
            $log.debug('cordovaGeolocationService.apiVersion.');
            return cordovaGeolocationConstants.apiVersion;
        },

        /**
         * Return the cordova API version.
         */
        cordovaVersion: function () {
            $log.debug('cordovaGeolocationService.cordovaVersion.');
            return cordovaGeolocationConstants.cordovaVersion;
        },

        /**
         * Check the geolocation plugin availability.
         * @returns {boolean}
         */
        checkGeolocationAvailability: function () {
            $log.debug('cordovaGeolocationService.checkGeolocationAvailability.');
            if (!navigator.geolocation) {
                console.log('Geolocation API is not available.');
                $log.warn('Geolocation API is not available.');
                return false;
            }
            else{
                 $log.debug('retorna true');
                  console.log('retorna true');
                  return true;
            }
          
        },

        /**
         * Returns the device's current position to the geolocationSuccess callback with a Position object as the parameter.
         * For more information: https://github.com/apache/cordova-plugin-geolocation/blob/master/doc/index.md#navigatorgeolocationgetcurrentposition
         */
        getCurrentPosition: function (successCallback, errorCallback, options) {
            $log.debug('cordovaGeolocationService.getCurrentPosition.');

            // Checking API availability
            if (!this.checkGeolocationAvailability()) {
                if(errorCallback!=null)
                $rootScope.$apply(errorCallback(""));
               // return;
            }
            else{

            // API call
            navigator.geolocation.getCurrentPosition(
                function (position) {
                    $rootScope.$apply(successCallback(position));
                },
                function (error) {
                     //alert("The following error occurred: " + error);
                     $ionicPopup.alert({
                           title: 'Atención',
                          template: 'Su GPS se encuentra desactivado. Por favor ingrese en la configuración de su dispositivo y active los servicios de GPS.'
                      }).then;
                      if(errorCallback!=null)
                      $rootScope.$apply(errorCallback(error));
                },
                options
            );
              }
        },

        /**
         * Returns the device's current position when a change in position is detected.
         * For more information: https://github.com/apache/cordova-plugin-geolocation/blob/master/doc/index.md#navigatorgeolocationwatchposition
         */
        watchPosition: function (successCallback, errorCallback, options) {
            $log.debug('cordovaGeolocationService.watchPosition.');

            // Checking API availability
            if (!this.checkGeolocationAvailability()) {
                return;
            }

            // API call
            return navigator.geolocation.watchPosition(
                function (position) {
                    $rootScope.$apply(successCallback(position));
                },
                function (error) {
                    $rootScope.$apply(errorCallback(error));
                },
                options
            );
        },

        /**
         * Stop watching for changes to the device's location referenced by the watchID parameter.
         * For more information: https://github.com/apache/cordova-plugin-geolocation/blob/master/doc/index.md#navigatorgeolocationclearwatch
         */
        clearWatch: function (watchID) {
            $log.debug('cordovaGeolocationService.clearWatch.');

            // Checking API availability
            if (!this.checkGeolocationAvailability()) {
                return;
            }

            // API call
            navigator.geolocation.clearWatch(watchID);
        }
    };
}])
.factory('CompradoresDataService', function($q, $timeout) {
    var searchCompradores = function(searchFilter,stateTab) {
              console.log("FUCKING ENTRY : "+searchFilter); 
              if(!angular.equals(searchFilter,"")){
              if(stateTab){
               var matches = compradores.filter(function(airline) {
                     console.log("FUCKING RESULT : "+angular.toJson(airline));
                    if(airline.Producto!=null) {
                       if(airline.Producto.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
                    }

               })
               $timeout( function(){
                   deferred.resolve( matches );
               }, 100);
             }
             else
             {
              var matches = ventas.filter(function(airline) {
                     console.log("FUCKING RESULT  IN VENTAS: "+angular.toJson(airline));
                    if(airline.Producto!=null) {
                       if(airline.Producto.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
                    }

               })
               $timeout( function(){
                   deferred.resolve( matches );
               }, 100);
             }
           }
           else
           {
              var matches = "";
               $timeout( function(){
                   deferred.resolve( matches );
               }, 100);

           }
        var deferred = $q.defer();
        return deferred.promise;
    };
    return {
        searchCompradores : searchCompradores
    }
})
.factory('MunicipiosDataService', function($q, $timeout,GetMunicipios) {
    var searchMunicipios = function(searchFilter) {
        console.log("search filter: "+ angular.toJson(searchFilter));
        GetMunicipios.getMunis(searchFilter).success(function(response){
             municipios = response.Data;
             var matches = municipios.filter(function(item) {
                if(item.Nombre.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
            })
            $timeout( function(){
               deferred.resolve( matches );
            }, 100);
        
        }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });  
        var deferred = $q.defer();
        return deferred.promise;
    };
    return {
        searchMunicipios : searchMunicipios
    }
})
.factory('VariedadesDataService', function($q, $timeout,obtenerVariedades) {
    var searchVariedades = function(id,searchFilter) {
        console.log("search filter: "+ angular.toJson(searchFilter)+" ID DATA: "+id);
        obtenerVariedades.get(id,searchFilter).success(function(response){
             variedades = response.Data;
             var matches = variedades.filter(function(item) {
                if(item.Nombre.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
            })
            $timeout( function(){
               deferred.resolve( matches );
            }, 100);
        
        });  
        var deferred = $q.defer();
        return deferred.promise;
    };
    return {
        searchVariedades : searchVariedades
    }
})
.factory('PresentacionesDataService', function($q, $timeout,obtenerVariedades) {
    var searchPresentaciones = function(searchFilter) {
        console.log("search filter: "+ searchFilter);
             var matches = presentaciones.filter(function(item) {
                console.log("RESULT SEARCH : "+angular.toJson(item))
                if(item.Nombre.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
            })
             $timeout( function(){
               deferred.resolve( matches );
            }, 100); 
        var deferred = $q.defer();
        return deferred.promise;
    };
    return {
        searchPresentaciones : searchPresentaciones
    }
})
.factory('InfoService', ['$window', function($window){
	console.log(".factory(InfoService)");
    var arrayDataAll = [];
    var contentData = "Prueba Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore eu Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore euLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore euLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore euLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore euLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore euLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore eu Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod cillum dolore eu";
    //Incio
    var arrayDataT = [];
    var arrayDataN = [];
    var arrayDataP = [];
    //Favoritos
    var favoriteAll = [];
    var favoriteT = [];
    var favoriteP = [];
    var favoriteN = [];
    //DataUser    
    var userData = angular.fromJson($window.localStorage['dataUser'] || "{}") ;
    //Favorite
    var favoriteAll = angular.fromJson($window.localStorage['favortieData'] || "[]") ;
    for (var i = 0; i < favoriteAll.length; i++){
        console.log("Organizacion Favoritos");
        selectArrayCategoryFavorite(favoriteAll[i]);
    } 
    //arrayDataAll.push(dataTramite)   
    //arrayDataAll.push(dataProgram)  
    //arrayDataAll.push(dataNotice)
    /*for (var i = 0; i < arrayDataAll.length; i++){
        selectArrayCategory(arrayDataAll[i])    
    }*/
    var insertFavoriteData = function(object){

        console.log("Insertando object");
        favoriteAll.push(object);
        createLocalStoregeFavorites();
        switch(object.category) {

        case "Tramite":
            favoriteT.push(object);
            break;

        case "Programa":
            favoriteP.push(object);
            break;

        case "Noticia":
            favoriteN.push(object);
            break;

        default:
            alert("No existe");
            break;
        }
    }
    var deteleObjectFavorite = function(object){
        //Eliminar del array General
        console.log("Eliminar services object")

        for (var i = 0; i < favoriteAll.length; i++){
            if (object.idObjec == favoriteAll[i].idObjec){
                favoriteAll.splice(i,1);
                
            }
        }
        createLocalStoregeFavorites()
        switch(object.category) {

        case "Tramite":
               
                for (var i = 0; i < favoriteT.length; i++){
                    if (object.idObjec == favoriteT[i].idObjec){
                        favoriteT.splice(i,1);
                       
                        return
                     }
                }

            break;

        case "Programa":
                for (var i = 0; i < favoriteP.length; i++){
                    if (object.idObjec == favoriteP[i].idObjec){
                        favoriteP.splice(i,1);
                        return
                     }
                }
            break;

        case "Noticia":
                for (var i = 0; i < favoriteN.length; i++){
                    if (object.idObjec == favoriteN[i].idObjec){
                        favoriteN.splice(i,1);
                        return
                     }
                }
            break;

        default:
            alert("No existe")
            break;
        }
    }
    // Categorias del LocalStorage Favoritos
    function selectArrayCategoryFavorite(object){

        switch(object.category) {

        case "Tramite":
            favoriteT.push(object);
            break;

        case "Programa":
            favoriteP.push(object);
            break;

        case "Noticia":
            favoriteN.push(object);
            break;

        default:
            alert("No existe");
            break;
        }
    }
    // Categorias del LocalStorage Data
    function selectArrayCategory(object){

        switch(object.category) {

        case "Tramite":
            arrayDataT.push(object);
            break;

        case "Programa":
            arrayDataP.push(object);
            break;

        case "Noticia":
            arrayDataN.push(object);
            break;

        default:
            alert("No existe");
            break;
        }
    }
    // Crear LocalStorege Favoritos
    function createLocalStoregeFavorites(){
        $window.localStorage['favortieData'] = angular.toJson(favoriteAll) ;  
    }

    function recoverFavoritesLocalStorege(){
        return true;
    }
    function deleteDB(){
        console.log("Eliminar");
        $window.localStorage.removeItem('dataUser');
        $window.localStorage.removeItem('favortieData');
    }
    //Services Return
    return {

        getDataT: function(){
            return arrayDataT;
        },

        getDataP: function(){
            return arrayDataP;
        },

        getDataN: function(){
            return arrayDataN;
        },

        getDataAll: function(){
            return arrayDataAll;
        },

        getObjectById: function(idObjec){
            for (  var i = 0;  i<arrayDataAll.length; i++ ) {
                     console.log(arrayDataAll[i].idObjec);
                if ( idObjec == arrayDataAll[i].idObjec) {
                    return arrayDataAll[i];
                }
            }
            return undefined;
        },
        //Favoritos
        getFavoriteT: function(){
            return favoriteT;
        },

        getFavoriteP: function(){
            return favoriteP;
        },

        getFavoriteN: function(){
            return favoriteN;
        },

        getFavoriteAll: function(){
            return favoriteAll;
        },

        insertFavorite: function(object){        
            insertFavoriteData(object);
        },
        deleteFavorite: function(object){
            deteleObjectFavorite(object);
        },

        // LocalStore
        createDataUser: function(userLocal){     
            console.log("createDataUser is creating with data : "+angular.toJson(userLocal))     ;  
            $window.localStorage['dataUser'] = angular.toJson(userLocal); 
        },
        createToken: function(token){     
            console.log(token) ;      
            $window.localStorage['tokenUser'] = angular.toJson(token) ;
        },
        getDataUser: function(){       
            return userData;
        },
        deleteDB: function(){
            deleteDB();
        }
    }
}]);