angular.module('starter.favoriteCtrl',[])
.controller('favoriteCtrl',['$scope','$filter','$ionicLoading', '$state','$ionicHistory','InfoService','$ionicScrollDelegate','$localstorage','$ionicPopup','getFavoritos','getFavoritosVenta','removeFavorito',  function($scope,$filter,$ionicLoading,$state,$ionicHistory,InfoService,$ionicScrollDelegate,$localstorage,$ionicPopup,getFavoritos,getFavoritosVenta,removeFavorito){ 
    console.log("Inicia favoriteCtrl");

    $scope.arrayData = InfoService.getDataAll()
    $scope.tabSelect=true;
    var dataUs=$localstorage.get('tokenUser')
    $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; "
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "

    var arrayDataT = InfoService.getDataT()
    var arrayDataP = InfoService.getDataP()
    var arrayDataN = InfoService.getDataN()
    var arrayDataAll = InfoService.getDataAll()
    var customers = {}
    $scope.isVenta=false;
    $scope.misfavoritebuy = { "misfavcompradores" : [] };
    $scope.misfavoritesold = { "misfavventas" : [] };
    $scope.actionInfo = function(tag){

        $scope.arrayData = [] 
        if(tag == 1 ) {     
            $scope.arrayData = []
            $scope.arrayData = arrayDataAll
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "   
            $scope.tabSelect=true;
            $scope.isVenta=false;
        }
        if(tag == 2 ) {       
            $scope.arrayData = []
            $scope.arrayData = arrayDataT
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            $scope.tabSelect=false;
            $scope.isVenta=true;
        }
        
        $ionicScrollDelegate.scrollTop();
    }
    $scope.actionFavorite = function(object){       
        for(var i = 0; i < $scope.arrayData.length; i++){

            if ($scope.arrayData[i].idObjec == object.idObjec) {

                if ($scope.arrayData[i].stateFavorite){
                    //Eliminar
                    $scope.arrayData[i].iconFavorite = "ion-ios-star-outline"
                    $scope.arrayData[i].stateFavorite = false
                    $scope.arrayData[i].textStateFavorite = "Añadir a favoritos"
                    InfoService.deleteFavorite(object) 
                }else{
                    //Crear
                    $scope.arrayData[i].iconFavorite = "ion-ios-star"
                    $scope.arrayData[i].stateFavorite = true
                    $scope.arrayData[i].textStateFavorite = "Quitar de favoritos"
                    InfoService.insertFavorite(object)  
                }
            }
        }  
    }

     $scope.deleteFavorite = function(id){ 
        var dataUs=$localstorage.get('tokenUser')  
        $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });  
        removeFavorito.remove(id,dataUs,$scope.tabSelect).success(function(response){
            $ionicLoading.hide()
            var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Publicacion eliminada',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                                          
                                    $ionicHistory.nextViewOptions({
                                        disableAnimate: true,
                                        disableBack: true
                                      });
                                      $state.go('app.homeclass');
                         }
                        }
                       ]
                   });
        }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
    }
    //ITS WORKING
    $scope.goToDetail = function(idCompra){
      stateTabFav=$scope.tabSelect;
      fromFav=true;
      detailFrom=idCompra;
      $state.go('app.tabdetailclass');
    }
    $scope.loadAllData = function(){

          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Obteniendo mis favoritos....'
            });
            getFavoritos.get(dataUs).success(function(response){
              angular.forEach(response.Data, function(c, key) {
                 c.FechaEntrega=$filter('date')(c.FechaEntrega, 'dd/MM/yyyy')
              });
              $scope.misfavoritebuy.misfavcompradores = response.Data;
              getFavoritosVenta.get(dataUs).success(function(responsefav){
               angular.forEach(responsefav.Data, function(c, key) {
                        c.VigenciaHasta=$filter('date')(c.VigenciaHasta, 'dd/MM/yyyy')
                        c.TotalCost=c.Cantidad*c.PrecioUnitario
              });  
              $scope.misfavoritesold.misfavventas = responsefav.Data;
              $ionicLoading.hide()
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });;
          }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
    }
    $scope.loadAllData();
}])