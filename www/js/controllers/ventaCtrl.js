angular.module('starter.ventaCtrl',[])
.controller('ventaCtrl', function($scope,$ionicSlideBoxDelegate,$ionicScrollDelegate,$ionicHistory,$state,$ionicPopup,$localstorage,$filter,$ionicLoading,InfoService,obtenerListasPerfilCategorias,MunicipiosDataService,ConnectivityMonitor,cordovaGeolocationService,VariedadesDataService,PresentacionesDataService,crearVenta) {

    $scope.isMunicipio=false;
    $scope.isVariedades=false;
    $scope.isPresentaciones=false;
    $scope.count=0;
    var municipioLocal=0;
    var departamentoLocal=0;
    var cate=0
    var varId=0;
    var preId=0;

    $scope.categorias = [];
    $scope.presentaciones = [];
    $scope.municipiosdataregister = { "municipiosregister" : [] };
    $scope.muni={ title: ''};

    $scope.presentacionesbuf = { "presentaciones" : [] };
    $scope.present={ title: ''};

    $scope.vararray = { "variedades" : [] };
    $scope.variedad={ title: ''};
    $scope.date = new Date();
    $scope.currentPosition=null;

     $scope.userModel = {
       product : '',
       variety: '',
       cant:0,
       costuni:0,
       totalcost:0,
       date:new Date(),
       coment:'',
       finalcoment:'',
       category:'',
       VigenciaDesde:$filter('date')(new Date(), 'dd/MM/yyyy'),
       VigenciaHasta:$filter('date')(new Date(), 'dd/MM/yyyy'),
       FechaActualiza:$filter('date')(new Date(), 'dd/MM/yyyy'),
       FechaCreacion:$filter('date')(new Date(), 'dd/MM/yyyy'),
       republicar:0,
       estado:0,
       unidad:0,
       municipioId:0
    }

    $scope.goToSold = function(){
      $state.go('app.sold');
    }

    $scope.goToBuy = function(){
      $state.go('app.buy');
    }
    $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
    obtenerListasPerfilCategorias.register().success(function(responsec){
                    var dataresc=responsec;
                    $ionicLoading.hide();
                    if(dataresc.Estado==1) {
                       console.log("SET CATEGORY ARRAY");
                       $scope.categorias =  dataresc.Data.Categorias;
                       $scope.uMedida =  dataresc.Data.Medidas;
                       $scope.presentaciones=dataresc.Data.Presentaciones;
                       presentaciones=$scope.presentaciones;
                       console.log("SET PRESENTATION ARRAY");
                    }else{
                              $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No hay datos por mostrar.'
                                }).then(function() {
                                  
                                }); 

                    }
   }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });

   $scope.searchMunicipios = function(muni) {
   $scope.isMunicipio=true;
   console.log("ENTRA : "+muni);
   MunicipiosDataService.searchMunicipios(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.municipiosdataregister.municipiosregister = matches;
             //$scope.scrollTop()
        }
      )
    }

   $scope.searchPresentaciones = function(muni) {
   $scope.isPresentaciones=true;
   console.log("ENTRA : "+muni);
   if(muni!=null){
   PresentacionesDataService.searchPresentaciones(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.presentacionesbuf.presentaciones = matches;
             //$scope.scrollTop()
        }
      )
    }
    else
    {
      $scope.isPresentaciones=false;
    }
  }

    $scope.searchVariedad = function(muni) {
      if(cate!=0)
      {
           $scope.isVariedades=true;
           console.log("ENTRA : "+muni);
           VariedadesDataService.searchVariedades(cate,muni).then(
           function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.vararray.variedades = matches;
             //$scope.scrollTop()
           })
      }
      else
      {
        if(cont==0)
        {
          cont=1;
          var myPopup = $ionicPopup.show({
          title: 'Atencion',
          template: 'Debe seleccionar una categoria de producto',
          scope: $scope,
          buttons: [
          {
              text: '<b>Ok</b>',
              type: 'button-positive',
              onTap: function(e) {
                  cont=0;
               }
             }
            ]
          });
        }

      }
    }
   
    $scope.seleccionar = function(item){
      console.log("SELECTED P: "+angular.toJson(item));
      this.muni.title=item.Nombre+" "+item.Departamento;
      municipioLocal=item.Id;
      departamentoLocal=item.IdDepartamento;
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }

     $scope.seleccionarVar = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.variedad.title=item.Nombre;
         varId=item.Id;
         console.log("SELECTED ID : "+varId);
         $scope.isVariedades  = false;
    }

     $scope.seleccionarPre = function(item){
         console.log("SELECTED P: "+angular.toJson(item));
         this.present.title=item.Nombre;
         preId=item.Id;
         console.log("SELECTED ID : "+preId);
         $scope.isPresentaciones  = false;
    }

    $scope.seleccionarCatego = function(id){
       console.log("SELECTED ID CATE: "+id);
       angular.forEach($scope.categorias, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);
                     }
       });
    }

    $scope.seleccionarMedida = function(id){
       console.log("SELECTED ID Medida: "+id);
       angular.forEach($scope.uMedida, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);
                     }
       });
    }
    var dataUs=$localstorage.get('tokenUser')
    console.log("TOKEN  : "+angular.toJson(dataUs))
    $scope.inicialdate = $filter('date')(new Date(), 'dd/MM/yyyy');
   
    $scope.actionButtonPublicar = function(theForm,form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("IS ONLINE?? "+onLine);
      if(true)//if(onLine)
      {
         if(cate!=0 && varId!=0 && municipioLocal!=0 && preId!=0) {
            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
           console.log("ID USER : "+dataUs)
           $scope.date=$filter('date')($scope.userModel.date, 'dd/MM/yyyy');
           console.log("NEW DATE TIME : "+$scope.date);
            if($scope.currentPosition!==undefined){
               cLatitud = $scope.currentPosition.coords.latitude.toString();
               cLongitud = $scope.currentPosition.coords.longitude.toString();
            }
            else{
              cLatitud=0.0;
              cLongitud=0.0;
            }
           crearVenta.createventa($scope.userModel.cant,$scope.userModel.costuni,
            $scope.userModel.VigenciaDesde,$scope.date ,$scope.userModel.finalcoment,$scope.userModel.FechaCreacion,
            $scope.userModel.FechaActualiza,$scope.userModel.republicar,cLatitud,
            cLongitud,dataUs,varId,$scope.userModel.estado,$scope.userModel.unidad,preId,
            municipioLocal).success(function(response){
               var datares=response;
               if(datares.Estado== 1) {
                 //$state.go('app.login-result') 
                 $ionicLoading.hide()
                  var myPopupExit = $ionicPopup.show({
                     title: 'Atencion',
                     template: 'Su publicacion sera aprobada muy pronto',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                              //$scope.cleanStack()

                                    $ionicHistory.nextViewOptions({
                                        disableAnimate: true,
                                        disableBack: true
                                      });
                                      $state.go('app.homeclass');
                         }
                        }
                       ]
                   });
               }
               else{
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                }).then;
              }
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });

          }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            }).then;
        }
      }
     
      $ionicScrollDelegate.scrollTop()
      $scope.stateRegistre = true
      $scope.stateSugesstion = false
      $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
      $scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
        
    }

    $scope.getCurrentPosition = function () {
            cordovaGeolocationService.getCurrentPosition(successHandler);
    };
    $scope.startWatchingPosition = function () {
            $scope.watchId = cordovaGeolocationService.watchPosition(successHandler);
    };
    $scope.stopWatchingPosition = function () {
            cordovaGeolocationService.clearWatch($scope.watchId);
            $scope.watchId = null;
            $scope.currentPosition = null;
    };
    // Handlers
    var successHandler = function (position) {
            $scope.currentPosition = position;
            //alert("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
            //console.log("POSITION : "+$scope.currentPosition.coords.latitude+"  LONGITUD : "+$scope.currentPosition.coords.longitude);
    };
     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }

    $scope.getCurrentPosition();
    $scope.change = function(data){
         console.log("RESULT : "+data+" MULTIPLICADO :"+$scope.userModel.cant)
         if(data!=null)
           $scope.userModel.totalcost=$scope.userModel.cant*data;
         else
           $scope.userModel.totalcost=0;
    }
     $scope.nextSlide = function() {
       $ionicSlideBoxDelegate.next();
       console.log("fucckkk xxx: "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    }

    $scope.previusSlide = function() {
       $ionicSlideBoxDelegate.previous();
       console.log("fucckkk : "+$scope.count);
       $ionicScrollDelegate.scrollTop();
    } 
     $scope.slideHasChanged = function($index){
          $scope.count=$index;
          console.log("fucckkk yyy: "+$scope.count);
          $ionicScrollDelegate.scrollTop();
          $scope.slideIndex = $index;
      
    };

});