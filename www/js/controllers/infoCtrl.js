angular.module('starter.infoCtrl',[])
.controller('infoCtrl',
            ['$scope','$ionicLoading','$ionicPopup','$localstorage','$filter', '$state','$ionicHistory','InfoService','MunicipiosDataService','cordovaGeolocationService','$ionicScrollDelegate','CompradoresDataService','GetCompradores','GetVendedores', 'GetCityDefault','FunSipsa','PreciosSipsa','PreciosFinca','ConnectivityMonitor','$stateParams', 'GetLocation',
      function($scope,$ionicLoading,$ionicPopup,$localstorage,$filter,$state,$ionicHistory,InfoService,MunicipiosDataService,cordovaGeolocationService,$ionicScrollDelegate,CompradoresDataService,GetCompradores,GetVendedores,GetCityDefault,FunSipsa,PreciosSipsa,PreciosFinca,ConnectivityMonitor,$stateParams, GetLocation){ 
    $scope.arrayData = InfoService.getDataAll();
    $scope.noMoreItemsAvailable = false;
    $scope.data = { "compradores" : [] };
    $scope.data.sold = { "ventas" : [] };
    $scope.data.municipiosdata = { "municipios" : [] };
    $scope.data.sipsa = { "productos" : [] };
    $scope.isVenta=false;
    $scope.isPrecios=false;
    $scope.islabel=false;
    $scope.nameMuni="";
    //$scope.data.searchmunicipios=$scope.nameMuni;
    //console.log("RESULLL"+$scope.data.searchmunicipios);

   /**/ $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; ";
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; ";
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; ";
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; ";

    var arrayDataT = InfoService.getDataT();
    var arrayDataP = InfoService.getDataP();
    var arrayDataN = InfoService.getDataN();
    var arrayDataAll = InfoService.getDataAll();
    var customers = {};
    $scope.isMunicipio=false;

     /*PreciosSipsa.getSipsa("Bogota","Cafe").then(function(responsecp){
                   var datarescp=responsecp.data;
                   if(datarescp.Data.length>0){
                        angular.forEach(datarescp.Data, function(c, key) {
                              c.Fecha=$filter('date')(c.Fecha, 'dd/MM/yyyy')
                       });
                       $scope.sipsa.precios = datarescp.Data;
                       PreciosFinca.getSipsa(municipio).then(function(responsec){
                           var dataresc=responsec.data;
                              if(dataresc.Estado==1) {
                                  $scope.sipsafinca.preciosfinca=dataresc.Data;
                                  $ionicLoading.hide();
                              }
                              else
                              {
                                $ionicLoading.hide();

                              }
                       });
                   }
    });*/

    $scope.actionInfo = function(tag){

        $scope.arrayData = [] ;
        if(tag == 1 ) {    
            $scope.arrayData = [];
            $scope.arrayData = arrayDataAll;
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #5b9bfb; color: white; ";
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; ";
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; ";
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; ";
          if(desde>0)  
            desde=0;
          stateTab=true;
          $scope.isVenta=false;
          $scope.isPrecios=false;
          $scope.data.search="";
          $scope.search();
        }
        if(tag == 2 ) {    
            $scope.arrayData = [];
            $scope.arrayData = arrayDataT;
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; ";
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #5b9bfb; color: white; ";
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; ";
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; ";  
          if(desde>0)  
            desde=0;
          stateTab=false;
          $scope.isVenta=true;
          $scope.isPrecios=false;
          $scope.data.search="";
          $scope.search();
        }
        if(tag == 3 ) {    //CHANGE     
            $scope.arrayData = [];
            $scope.arrayData = arrayDataP;
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; ";
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; ";
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #5b9bfb; color: white; ";
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; ";
            if(desde>0)  
               desde=0;
            stateTab=false; 
            $scope.isVenta=false;
            $scope.isPrecios=true;
            $scope.data.search="";
            $scope.search();
            console.log("Setting true "+$scope.isPrecios);
        }
        //$ionicScrollDelegate.scrollTop();
    }

    $scope.search = function() {

                          if(stateTab){
                                    $scope.data.compradores=[]; 
                                    compradores=[];
                                     compradoresbuf=[];
                                    $scope.scrollTop();
                                    desde=0;
                                    loadingDataCompradores = false;
                                    console.log("borrando data");
                              }
                              else{
                                  desdeVenta=0;
                                  $scope.data.sold.ventas=[];
                                  ventas=[];
                                  ventasbuf=[];
                                   $scope.scrollTop();
                                  loadingDataVendedores = false;
                                }


       if(!$scope.isPrecios){
          console.log("stateTab: "+stateTab);
          if(stateTab){
                        desdeVenta=0;
                        $scope.loadMoreData(); 

            }
            else{
              desdeVenta=0;
              $scope.loadMoreData(); 
              

            }

      }
      else
      {//CHANGE
        var nameSplit = nameX.split(" ");
        console.log("is true "+nameSplit[0]);
          $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
              });
        FunSipsa.getSipsa(nameSplit[0],$scope.data.search).success(
               function(matches) {
                $ionicLoading.hide();
                   console.log("la respuesta SIPSA fue: "+ angular.toJson(matches.Data));

                   if(matches.Data!=null &&matches.Data.length>0){
                      $scope.data.sipsa.productos = matches.Data;
                       precios=$scope.data.sipsa.productos;
                       $scope.islabel=true;
                       $scope.scrollTop();
                   }
                   else{
                    $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No se obtuvo datos.'
                                }).then(function() {
                                  
                                }); 
                   }
                 }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
      }
    }
    $scope.searchMunicipios = function() {
   

    if(!angular.equals($scope.data.searchmunicipios,""))  
      $scope.isMunicipio=true;
    else
      $scope.isMunicipio=false;
        console.log("THA FUCKK : "+$scope.data.searchmunicipios)
        MunicipiosDataService.searchMunicipios($scope.data.searchmunicipios).then(
        function(matches) {
             console.log($scope.data.searchmunicipios+"la respuesta fue: "+ angular.toJson(matches));
             $scope.data.municipiosdata.municipios = matches;
                 $scope.data.compradores=[];
                  //$scope.scrollTop();
                  desde=0;
                  municipio=0;
                  
                   
                   if($scope.data.searchmunicipios===''){
                             if(stateTab){
                                    $scope.data.compradores=[]; 
                                    compradores=[];
                                     compradoresbuf=[];
                                    $scope.scrollTop();
                                    desde=0;
                                    loadingDataCompradores = false;
                                    console.log("ojo ID : ");
                                   
                              }
                              else{
                                  desdeVenta=0;
                                  $scope.data.sold.ventas=[];
                                  ventas=[];
                                  ventasbuf=[];
                                   $scope.scrollTop();
                                  loadingDataVendedores = false;
                                }
                          $scope.noMoreItemsAvailable = false;
                          $scope.loadMoreData(); 

                   }
          //        
        }
      )
    }
    $scope.actionFavorite = function(object){       
        for(var i = 0; i < $scope.arrayData.length; i++){

            if ($scope.arrayData[i].idObjec == object.idObjec) {

                if ($scope.arrayData[i].stateFavorite){
                    //Eliminar
                    $scope.arrayData[i].iconFavorite = "ion-ios-star-outline"
                    $scope.arrayData[i].stateFavorite = false
                    $scope.arrayData[i].textStateFavorite = "Añadir a favoritos"
                    InfoService.deleteFavorite(object) 
                }else{
                    //Crear
                    $scope.arrayData[i].iconFavorite = "ion-ios-star"
                    $scope.arrayData[i].stateFavorite = true
                    $scope.arrayData[i].textStateFavorite = "Quitar de favoritos"
                    InfoService.insertFavorite(object)  
                }
            }
        }  
    }
    $scope.seleccionar = function(item){
      console.log("SELECTED : "+angular.toJson(item));
      $scope.data.searchmunicipios=item.Nombre+" "+item.Departamento;
      municipio=item.Id;
      idDepartamento=item.IdDepartamento;
      console.log("SELECTED ID : "+municipio);
      $scope.isMunicipio  = false;
       if(!$scope.isPrecios){

       if(stateTab){
            $scope.data.compradores=[]; 
            compradores=[];
             compradoresbuf=[];
            $scope.scrollTop();
            desde=0;
            loadingDataCompradores = false;
            console.log("ojo ID : ");
       
      }
      else{
          desdeVenta=0;
          $scope.data.sold.ventas=[];
          ventas=[];
          ventasbuf=[];
           $scope.scrollTop();
          loadingDataVendedores = false;
        }
      
       $scope.noMoreItemsAvailable = false;
       $scope.loadMoreData();   
      }
    }
    $scope.goToDetail = function(idCompra){
      //idBuy=idCompra;
      detailFrom=idCompra
      $state.go('app.tabdetailclass');
    }

    $scope.goToPublish = function(idCompra){
       var dataUs=$localstorage.get('tokenUser')
       console.log("SETTING TOKEN "+dataUs)
       if(dataUs!=null)
         $state.go('app.publish');
       else
       {
           $ionicPopup.alert({
                title: 'Atención',
                template: 'No se encuentra registrado. Para usar esta opción, por favor, regístrese e intente de nuevo.'
            }).then;
       }
    }

    $scope.loadMoreData = function(){
    //   if(desde>0)
    //   {
       console.log("************"+JSON.stringify($state.params));

          if ( desde == 9 ) {
             $scope.noMoreItemsAvailable = true;
          }

          if(stateTab)
          {
            /**
            Revisa si hay datos guardados 
            */
                if (desde==0){
                  if($scope.data.compradores.length>0){
                   desde= $scope.data.compradores[$scope.data.compradores.length-1].Id;
                  }
                }
          
            if (!loadingDataCompradores)
            {
              loadingDataCompradores=true;
              $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
              });
              
                    GetCompradores.getBuyers(municipio,desde,  $scope.data.search).success(function(response){

                            angular.forEach(response.Data, function(c, key) {
                                 c.FechaEntrega=$filter('date')(c.FechaEntrega, 'dd/MM/yyyy')
                                 $scope.data.compradores.push(c);
                                 compradores.push(c);
                                 compradoresbuf.push(c);
                                 desde=c.Id+1;
                                 
                               
                              });
                            loadingDataCompradores=false;
                            if(response.Data.length==0){
                              loadingDataCompradores = true;
                              $scope.noMoreItemsAvailable = true;}
                            $ionicLoading.hide();
                        }).error(function(data,status, headers, config) {
                          console.log('Error de conexión, por favor intente nuevamente');
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  $ionicHistory.goBack();
                                });
                               

                        }); 
            }
            else{
                  $ionicLoading.hide();
                  //$scope.noMoreItemsAvailable = true;
              }
         }
         else
         {
          console.log("entro  desdeVenta!!!!"+desdeVenta);
           if(!$scope.isPrecios){
                    $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
                if (!loadingDataVendedores)
                {
                  loadingDataVendedores=true;
                      GetVendedores.getVende(municipio,desdeVenta,  $scope.data.search).success(function(response){
                            if(response.Data!=null){


                                angular.forEach(response.Data, function(c, key) {
                                            c.VigenciaHasta=$filter('date')(c.VigenciaHasta, 'dd/MM/yyyy')
                                            c.TotalCost=c.Cantidad*c.PrecioUnitario
                                            $scope.data.sold.ventas.push(c);
                                            ventas.push(c);
                                          //  ventasbuf.push(c);
                                            desdeVenta=c.Id+1;
                                            
                                             
                            });  
                            loadingDataVendedores=false;
                            if(response.Data.length==0){
                              loadingDataVendedores = true;
                              $scope.noMoreItemsAvailable = true;}

                            $ionicLoading.hide();
                              //desdePrecio=desdePrecio+1;
                            } 
                            else{
                                    $ionicLoading.hide();
                                $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No hay publicaciones por ver.'
                                }).then(function() {
                                  
                                });
                            }
                      }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                });

                               
                                
                        });;
                }
                else{
                  $ionicLoading.hide();
                  //$scope.noMoreItemsAvailable = true;
                }
          }
         }
     //  }
       //else
      // {
        //desde=desde+1;
  
       //}
       $scope.$broadcast('scroll.infiniteScrollComplete'); 
    }
     $scope.loadAllData = function(){
          var onLineProfile=ConnectivityMonitor.checkConnection();
          if(onLineProfile)
          {
            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Obteniendo Compradores y Vendedores....'
            });
            GetCompradores.getBuyers(municipio,desde).success(function(response){
              angular.forEach(response.Data, function(c, key) {
                        c.FechaEntrega=$filter('date')(c.FechaEntrega, 'dd/MM/yyyy');
              });
              $scope.data.compradores=response.Data;
              compradores=$scope.data.compradores;
              compradoresbuf=compradores;
              GetVendedores.getVende(municipio,desde).success(function(responseventa){
                    angular.forEach(responseventa.Data, function(c, key) {
                        c.VigenciaHasta=$filter('date')(c.VigenciaHasta, 'dd/MM/yyyy');
                        c.TotalCost=c.Cantidad*c.PrecioUnitario;
                    });  
                   $scope.data.sold.ventas=responseventa.Data;
                   ventas=$scope.data.sold.ventas;
                   ventasbuf=ventas;
                   GetCityDefault.get(latitude,longitude).success(function(responsegeo){
                          console.log("RESULT DATA : "+angular.toJson(responsegeo.Data));
                          municipio=responsegeo.Data.Id;
                          nameX=responsegeo.Data.Nombre+" "+responsegeo.Data.Departamento;
                          $scope.nameMuni=nameX;
                          console.log("ID BY DEFAULT : "+municipio+"   "+nameX);
                          $scope.data.searchmunicipios=$scope.nameMuni;
                          $ionicLoading.hide();
                    }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                });
                          });
                   ;
                   /*GetVendedores.getVende(municipio,desde).then(function(responsesipsa){
                    angular.forEach(responsesipsa.data.Data, function(c, key) {
                        c.VigenciaHasta=$filter('date')(c.VigenciaHasta, 'dd/MM/yyyy');
                    });  
                   $scope.data.sipsa.productos=responsesipsa.data.Data;
                   precios=$scope.data.sipsa.productos;
                   $ionicLoading.hide();
                 });*/
              }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                });
                          });;
          }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  $ionicHistory.goBack();
                                });

                               
                                
                        });
        }
    }
    $scope.scrollTop = function() {
        $ionicScrollDelegate.scrollTop();
    };
    $scope.scrollBot = function() {
        $ionicScrollDelegate.scrollBottom();
    };
     $scope.goToDetailHistory = function(idCompra){
      //idBuy=idCompra;
      detailFromPrecio=idCompra
      $state.go('app.tabdetailclasssipsa');
    }

   var errorHandler = function () {
              $scope.loadAllData();
              $scope.loadMoreData(); 
   }

    var posicion=true;
    $scope.getCurrentPosition = function () {
            $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Obteniendo Posición....'
            });

          cordovaGeolocationService.getCurrentPosition(successHandler, errorHandler)
          setTimeout(function(){ 
              posicion=false;
              $scope.stopWatchingPosition();
              $scope.loadAllData();
              $scope.loadMoreData();
             }, 10000);

    };
    $scope.startWatchingPosition = function () {
            $scope.watchId = cordovaGeolocationService.watchPosition(successHandler);
    };
    $scope.stopWatchingPosition = function () {
            cordovaGeolocationService.clearWatch($scope.watchId);
            $scope.watchId = null;
            $scope.currentPosition = null;
    };
    // Handlers

     


    var successHandler = function (position) {

            $scope.currentPosition = position;
            latitude=$scope.currentPosition.coords.latitude;
            longitude=$scope.currentPosition.coords.longitude;
            console.log("RESULT DATA GEO: "+latitude+"   "+longitude);
             if (posicion){
                GetLocation.getLocation(latitude, longitude).success(function(response)
                {
                 if (response.Data!=undefined){

                    console.log("RESULT  GEO: "+response.Data.Nombre);
                    $scope.data.searchmunicipios =response.Data.Nombre + " "+response.Data.Departamento;
                    municipio=response.Data.Id;
                    $scope.loadAllData();
                    $scope.loadMoreData(); 
                }
                
            

            }).error(function(data,status, headers, config) {
            
              $scope.loadAllData();
              $scope.loadMoreData(); 

            });
           }
    };




    $scope.getCurrentPosition();    
    
}])