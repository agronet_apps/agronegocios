angular.module('starter.DetailCtrl',[])
.controller('DetailCtrl',function($scope,$state,$filter,$localstorage,$ionicPopup,$ionicHistory,$cordovaSocialSharing,$cordovaEmailComposer,$ionicLoading,GetDetail,saveFavorito,obtenerListasPerfilCategorias, getFavoritosVenta, getFavoritos, removeFavorito) {
  $scope.detail ={};
  $scope.detail=detailFrom;
  $scope.presentaciones=[];
  $scope.categorias=[];
  var preId=0;
  $scope.valFav=false;
  $scope.favorito=false;
  $scope.preInfo="";
  $scope.contactar='';
  $scope.tipo='';
  if(fromFav){
    $scope.valFav=true;
    $scope.isVentaValidator=stateTabFav;
    fromFav=false;
    if(stateTabFav)
    {
      console.log("IS HERE 1");
      $scope.tituloDetalle='Compro '+ $scope.detail.Producto;
      $scope.tipo='comprador';
      $scope.contactar='Llamar al comprador';
      $scope.detail.FechaEntrega=$filter('date')($scope.detail.FechaEntrega, 'dd/MM/yyyy');

    }
    else
    {
      console.log("IS HERE 2");
      $scope.tituloDetalle='Vendo '+ $scope.detail.Producto;
      $scope.tipo='vendedor';
      $scope.contactar='Llamar al vendedor';
      $scope.detail.VigenciaHasta=$filter('date')($scope.detail.VigenciaHasta, 'dd/MM/yyyy');

    }
  }
  else{
     $scope.isVentaValidator=stateTab;
     if(stateTab)
     {
      console.log("IS HERE 3");
      $scope.tituloDetalle='Compro '+ $scope.detail.Producto;
      $scope.tipo='comprador';
      $scope.contactar='Llamar al comprador';
      $scope.detail.FechaEntrega=$filter('date')($scope.detail.FechaEntrega, 'dd/MM/yyyy');
      $scope.detail.TelefonoUsuario= $scope.detail.Telefono;
      if($scope.detail.Telefono===0){
           $scope.detail.TelefonoUsuario='No registrado'
      }
      
     }
     else
     {
      console.log("IS HERE 4");
      $scope.tituloDetalle='Vendo '+ $scope.detail.Producto;


      $scope.tipo='vendedor';
      $scope.contactar='Llamar al vendedor';
      $scope.detail.VigenciaHasta=$filter('date')($scope.detail.VigenciaHasta, 'dd/MM/yyyy');
  
     }
   }
  /*GetDetail.getBuyer(idBuy).then(function(response){
         //$scope.detail = response.Data;
         console.log("The response : "+angular.toJson(response.data.Data))
         $scope.detail = response.data.Data;
    });*/
    console.log("Data Input : "+angular.toJson($scope.detail));
    $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });  


    obtenerListasPerfilCategorias.register().success(function(responsec){
                    var dataresc=responsec;
                    if(dataresc.Estado==1) {
             
                       console.log("Es venta? "+$scope.isVentaValidator);
                       var dataUs=$localstorage.get('tokenUser');
                         $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
                          });
                       if($scope.isVentaValidator){
                         getFavoritos.get(dataUs).success(function(responsefav){
                           
                             if(responsefav.Estado===1){
                                 console.log("ID del articulo"+$scope.detail.Id);
                                
                                  angular.forEach(responsefav.Data, function(c, key) {
                                        if(c.Id===$scope.detail.Id){
                                            $scope.favorito=true;
                                            console.log("esta en favoritos");
                                        }
                                   }); 
                             }
                            
                            $ionicLoading.hide()
                          }).error(function(data,status, headers, config) {
                                             $ionicLoading.hide();
                                             $ionicPopup.alert({
                                                  title: 'Atención',
                                                  template: 'Error de conexión, por favor intente nuevamente.'
                                              }).then(function() {
                                                
                                              }); });;
                        }
                        else{
                              getFavoritosVenta.get(dataUs).success(function(responsefav){
                                   
                                     if(responsefav.Estado===1){
                                         console.log("ID del articulo"+$scope.detail.Id);
                                        
                                          angular.forEach(responsefav.Data, function(c, key) {
                                                if(c.Id===$scope.detail.Id){
                                                    $scope.favorito=true;
                                                    console.log("esta en favoritos");
                                                }
                                           }); 
                                     }
                                    
                                  $ionicLoading.hide()
                                  }).error(function(data,status, headers, config) {
                                                     $ionicLoading.hide();
                                                     $ionicPopup.alert({
                                                          title: 'Atención',
                                                          template: 'Error de conexión, por favor intente nuevamente.'
                                                      }).then(function() {
                                                        
                                                      }); });;
                        }


                       $scope.categorias =  dataresc.Data.Categorias;
                       $scope.presentaciones=dataresc.Data.Presentaciones;
                       console.log("SET PRESENTATION ARRAY");
                    }else{
                      $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'No hay datos por mostrar.'
                                }).then(function() {
                                  
                                }); 
                    }
   }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
     angular.forEach($scope.presentaciones, function(c, key) {
                     if(angular.equals(c.Id==preId)){
                        console.log("FOUND IT!!!: "+c.Id);
                        $scope.preInfo=c.Nombre
                        
                     }
       });

     $scope.saveFavorite = function(id){ 
         console.log("grabar favoritos");
         var dataUs=$localstorage.get('tokenUser')  
         $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });  
       if (!$scope.favorito){

              console.log("COMPRA ID: "+angular.toJson($scope.detail)) 
              saveFavorito.save( id,dataUs,stateTab).success(function(response){
                    var dataresc=response;
                        if(dataresc.Estado==1) {
                          $ionicLoading.hide()
                              var myPopupExit = $ionicPopup.show({
                           title: 'Atención',
                           template: 'Publicación agregada a favoritos',
                            scope: $scope,
                            buttons: [
                            {
                               text: '<b>Ok</b>',
                               type: 'button-positive',
                               onTap: function(e) {
                                        $ionicHistory.nextViewOptions({
                                              disableAnimate: true,
                                              disableBack: true
                                            });
                                            $state.go('app.homeclass');
                               }
                              }
                             ]
                         });
                      }
                      else
                      {
                        $ionicLoading.hide()
                          $ionicPopup.alert({
                            title: 'Resultado',
                            template: dataresc.Texto
                          }).then;
                      }
              }).error(function(data,status, headers, config) {
                                     $ionicLoading.hide();
                                     $ionicPopup.alert({
                                          title: 'Atención',
                                          template: 'Error de conexión, por favor intente nuevamente.'
                                      }).then(function() {
                                        
                                      }); });

      }
      else{
        console.log("SE VA A QUITAR DE FAVORTIOS");
        
        console.log("COMPRA ID: "+angular.toJson($scope.detail)) 
        removeFavorito.remove( id,dataUs,stateTab).success(function(response){
              var dataresc=response;
                  if(dataresc.Estado==1) {
                    $ionicLoading.hide()
                        var myPopupExit = $ionicPopup.show({
                     title: 'Atención',
                     template: 'Publicación eliminada de favoritos',
                      scope: $scope,
                      buttons: [
                      {
                         text: '<b>Ok</b>',
                         type: 'button-positive',
                         onTap: function(e) {
                                  $ionicHistory.nextViewOptions({
                                        disableAnimate: true,
                                        disableBack: true
                                      });
                                      $state.go('app.homeclass');
                         }
                        }
                       ]
                   });
                }
                else
                {
                  $ionicLoading.hide()
                    $ionicPopup.alert({
                      title: 'Resultado',
                      template: dataresc.Texto
                    }).then;
                }
        }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });







      }
    }




     $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
    }

   $scope.sendEmail = function(){
      var options = { 
        to: $scope.detail.Email,
             subject: '', 
             body: '', 
             isHtml: true};
      //$cordovaEmailComposer.
     cordova.plugins.email.open(options, function () {
    console.log('email !!!!');}, $scope);


  };


  $scope.shareAnywhere = function() {
         $cordovaSocialSharing.share("Oferta recomendada el dia de hoy: "+$scope.detail.Descripcion+". Ubicada en: "+$scope.detail.Municipio, "Agronegocios", "www/img/agro.png", "http://www.ministerioagricultura.com")
            .then(function(result) {
              console.log("success SOCIAL SHARING: "+ result) 
            }, function(err) {
              console.log("ERROR SOCIAL SHARING: "+ err) 
            });
         console.log("ENTER TO SOCIAL SHARING")  
  }

})