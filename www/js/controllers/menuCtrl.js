angular.module('starter.menuCtrl',[])
.controller('menuCtrl', function($scope,$cordovaSplashscreen,$ionicLoading, $ionicScrollDelegate,$ionicHistory,$state,$ionicPopup,$localstorage,InfoService,obtenerListasPerfil,obtenerListasPerfilCategorias,MunicipiosDataService,completarPerfil,obtenerPerfil,ConnectivityMonitor,CambiarClave ){
    $scope.stateRegistre = true;
    $scope.stateSugesstion = false;
    $scope.isMunicipio=false;
    $scope.isMunicipiop=false;

    var cate=0;
    var sex="";
    var level=0;
    var aso=0;
    var act=0;
    var asoChar='';

    $scope.activeBorderColorR = "border-color: #2F6850; background-color: #5b9bfb; color: white; ";
    $scope.activeBorderColorS = "border-color: #59595c; background-color: #f2f2f2; color: #59595c;";
    $scope.nivelEducativo = [];
    $scope.ActividadesAgricolas = [];
    $scope.categorias = [];
    $scope.municipiosdataregister = { "municipiosregister" : [] };
    $scope.municipiosdataregisterp = { "municipiosregisterp" : [] };
    $scope.muni={ title: ''};
    $scope.munip={ titlep: ''};

    $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            });
    obtenerListasPerfil.register().success(function(response){
            
               var datares=response;
               if(datares.Estado== 1) {
                 $scope.nivelEducativo =  datares.Data.NivelesEducativos;
                 $scope.ActividadesAgricolas =  datares.Data.ActividadesAgricolas;
               }
    }).error(function(data,status, headers, config) {
      
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
    obtenerListasPerfilCategorias.register().success(function(responsec){
      
                    var dataresc=responsec;
                    if(dataresc.Estado==1) {
                       console.log("SET CATEGORY ARRAY");
                       $scope.categorias =  dataresc.Data.Categorias;
                    }
   }).error(function(data,status, headers, config) {
    
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
    $scope.searchMunicipios = function(muni) {
    $scope.isMunicipio=true;
    console.log("ENTRA : "+muni);
    MunicipiosDataService.searchMunicipios(muni).then(
        function(matches) {
             console.log("la respuesta fue: "+ angular.toJson(matches));
             $scope.municipiosdataregister.municipiosregister = matches;
             //$scope.scrollTop()
        }
      )
    }
    $scope.searchMunicipiosP = function(munip) {
   $scope.isMunicipiop=true;
    console.log("ENTRA P: "+munip);
    MunicipiosDataService.searchMunicipios(munip).then(
        function(matches) {
             console.log("la respuesta fue P: "+ angular.toJson(matches));
             $scope.municipiosdataregisterp.municipiosregisterp = matches;

             //$scope.scrollTop()
        }
      )
    }
    var municipioLocal=0;
    var municipioLocalp=0;
    var departamentoLocal=0;
    var departamentoLocalp=0;
    $scope.seleccionar = function(item){
      console.log("SELECTED : "+angular.toJson(item));
      this.muni.title=item.Nombre+" "+item.Departamento;
      municipioLocal=item.Id;
      departamentoLocal=item.IdDepartamento;
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }

     $scope.seleccionarp = function(item){
      console.log("SELECTED P: "+angular.toJson(item));
      this.munip.titlep=item.Nombre+" "+item.Departamento;
      municipioLocalp=item.Id;
      municipioLocalname=item.Nombre;
      departamentoLocalp=item.IdDepartamento;
      console.log("SELECTED ID  P: "+municipioLocalp);
      $scope.isMunicipiop  = false;
    }

    $scope.seleccionarCatego = function(id){
       console.log("SELECTED ID CATE: "+id);
       angular.forEach($scope.categorias, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);
                     }
       });
    }
    $scope.seleccionarSex = function(id){
       sex=id;
       $scope.userModel.genero=sex;
       console.log("SELECTED ID SEX: "+sex+" vs  "+$scope.userModel.genero);
    }
    $scope.seleccionar = function(item){
      console.log("SELECTED : "+angular.toJson(item));
      this.muni.title=item.Nombre+" "+item.Departamento;
      municipioLocal=item.Id;
      departamentoLocal=item.IdDepartamento;
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }
    /*$scope.seleccionarAsochar = function(id){
       console.log("SELECTED ID ASOCHAR: "+id);
       angular.forEach($scope.categorias, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        cate=c.Id
                        console.log("SELECT ID!!!: "+cate);
                     }
       });
    }*/
    $scope.seleccionarActividad = function(id){
        console.log("SELECTED ID ACT: "+id);
         angular.forEach($scope.ActividadesAgricolas, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        act=c.Id
                        console.log("SELECT ID!!!: "+act);
                     }
          });
    }
    $scope.seleccionarNivel = function(id){
          console.log("SELECTED ID NIVEL: "+id);
          angular.forEach($scope.nivelEducativo, function(c, key) {
                     if(angular.equals(c.Nombre.toLowerCase(), id.toLowerCase())){
                        console.log("FOUND IT!!!: "+c.Id);
                        //$scope.userModel.category =c.Id;
                        level=c.Id
                        console.log("SELECT ID!!!: "+level);
                     }
          });
    }

     $scope.userModel = {
       name : '',
       email: '',
       documento:'',
       password:'',
       education:'',
       placeCity:'',
       cellPhone:'',
       genero:'',
       productionList:'',
       asociation:'',
       activity:'',
       category:'',
       productionPricipal:'',
       superfice:'',
       registre:false
    };
    var dataUs=$localstorage.get('tokenUser');
    console.log("TOKEN  : "+angular.toJson(dataUs));
    var onLineProfile=ConnectivityMonitor.checkConnection();
    console.log("IS ONLINE?? "+onLineProfile);

   // if(onLineProfile)
  //  {
      obtenerPerfil.get(dataUs).success(function(response){
        $ionicLoading.hide();
               var datares=response;
               if(datares.Estado== 1) {
                
              if(!angular.equals(datares.Data.Nombre,""))
                   $scope.userModel.name =datares.Data.Nombre+" "+datares.Data.SegundoNombre+" "+datares.Data.Apellido+" "+datares.Data.SegundoApellido;
              if(!angular.equals(datares.Data.Correo,""))
                 $scope.userModel.email =datares.Data.Correo;
              if(!angular.equals(datares.Data.DocIdentidad,""))
                 $scope.userModel.documento =datares.Data.DocIdentidad;
              if(!angular.equals(datares.Data.Superficie,""))
                 $scope.userModel.superfice =datares.Data.Superficie;
              if(!angular.equals(datares.Data.ProduccionPrincipal,""))
                  $scope.userModel.productionPricipal= datares.Data.ProduccionPrincipal;
              if(!angular.equals(datares.Data.MunicipioName,"")) 
                  {
                    $scope.muni.title= datares.Data.MunicipioName;
                  }
              if(!angular.equals(datares.Data.SuperficieUltimoYear,"")) 
                  {
                    $scope.userModel.SuperficieUltimoYear= datares.Data.SuperficieUltimoYear;
                  }
              if(!angular.equals(datares.Data.NumeroFamiliares,"")) 
                  {
                    $scope.userModel.NumeroFamiliares= datares.Data.NumeroFamiliares;
                  }
              if(!angular.equals(datares.Data.TrabajadoresPermanentes,"")) 
                  {
                    $scope.userModel.TrabajadoresPermanentes= datares.Data.TrabajadoresPermanentes;
                  }
              if(!angular.equals(datares.Data.TrabajadoresTemporales,"")) 
                  {
                    $scope.userModel.TrabajadoresTemporales= datares.Data.TrabajadoresTemporales;
                  }
              if(!angular.equals(datares.Data.DireccionProduccion,"")) 
                  {
                    $scope.userModel.LugarDeProduccionName= datares.Data.DireccionProduccion;
                  }
             
              


                  $scope.userModel.password ="";
             //   $scope.userModel.productionPricipal= datares.Data.ProduccionPrincipal;
                
                if(datares.Data.IdActividadAgricola!=0){
                 angular.forEach($scope.ActividadesAgricolas, function(c, key) {
                     if(c.Id==datares.Data.IdActividadAgricola){
                        console.log("ENTERING IdActividadAgricola: "+c.Nombre);
                        $scope.userModel.activity =c.Nombre;
                        act=datares.Data.IdActividadAgricola;
                     }
                 });
               }

                 obtenerListasPerfilCategorias.register().success(function(responsec){
                    var dataresc=responsec;
                    if(dataresc.Estado==1) {
                       console.log("SET CATEGORY ARRAY");
                       $scope.categorias =  dataresc.Data.Categorias;
                       angular.forEach($scope.categorias, function(c, key) {
                             if(c.Id==datares.Data.IdCategoriaProducto){
                               console.log("ENTERING IdCategoriaProducto: "+c.Nombre);
                               $scope.userModel.category =c.Nombre;
                               cate=datares.Data.IdCategoriaProducto;
                             }
                        });
                    }
                    else{
                      $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error general'
                                }).then(function() {
                                  
                                });
                    }
                 }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });;

                 angular.forEach($scope.nivelEducativo, function(c, key) {
                     if(c.Id==datares.Data.NivelEducativo){
                        console.log("ENTERING NivelEducativo: "+c.Nombre);
                        $scope.userModel.education =c.Nombre;
                        level=datares.Data.NivelEducativo;
                     }
                 });
                if(!angular.equals(datares.Data.Direccion,""))
                   $scope.userModel.placeCity =datares.Data.Direccion;
                 municipioLocal=datares.Data.IdMunicipio;
                 if(!angular.equals(datares.Data.NumeroCelular,""))
                   $scope.userModel.cellPhone =datares.Data.NumeroCelular;
                 if(!angular.equals(datares.Data.LugarDeProduccionName,""))
                   {
                    $scope.munip.titlep =datares.Data.LugarDeProduccionName;
                    municipioLocalname =datares.Data.LugarDeProduccionName;
                   } 

                 if(!angular.equals(datares.Data.LugarDeProduccionName,""))
                    municipioLocalp =datares.Data.LugarDeProduccion;


                 if(datares.Data.PerteneceAsociacion)
                    $scope.userModel.asociation ="SI";
                  else
                    $scope.userModel.asociation ="NO";
                 
            
                 if(!angular.equals(datares.Data.Genero,"")){
                    $scope.userModel.genero=datares.Data.Genero;
                    sex=$scope.userModel.genero;
                 }
               }
            }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  $scope.userModel = InfoService.getDataUser();;
                                }); });;
      
    //}
    //else
    //{

    //}
    $scope.buf="";
    $scope.actionButtonRegistre = function(form){
      var onLine=ConnectivityMonitor.checkConnection();
      console.log("perfil? "+onLine);
     // if(onLine)
     // {
         if(form.$valid && cate!=0 && level!=0  && act!=0 && municipioLocal!=0) {

          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
            }); 
           console.log("ID USER : "+dataUs);
           var name=$scope.userModel.name;
           
           var nameSplit = name.split(" ");
            console.log("nameSplit "+nameSplit);
           var firstName = "";
           var secondName = "";
           var lastName="";
           var secondLastName="";
           var finalName="";
           var secondFinalName="";
           var finalLastName="";
           var totaData="";
           if(nameSplit.length==4){
             firstName = nameSplit[0];
             secondName = nameSplit[1];
             lastName = nameSplit[2];
             secondLastName = nameSplit[3];
             finalName=firstName;
             secondFinalName=secondName;
             finalLastName=lastName+" "+secondLastName;
           }
           else
           {
              if(nameSplit.length==3)
              {
                firstName = nameSplit[0];
                secondName = nameSplit[1];
                lastName = nameSplit[2];
                finalName=firstName;
                secondFinalName=secondName+" "+lastName;
              }
              else{
                if(nameSplit.length==2)
                {
                   firstName = nameSplit[0];
                   secondName = nameSplit[1];
                   finalName=firstName;
                   secondFinalName=secondName;
                }
                else{
                  finalName =nameSplit[0];
                }
              }

           }
           totaData=finalName+" "+secondFinalName+" "+finalLastName;
          console.log("LugarDeProduccionName: "+$scope.userModel.LugarDeProduccionName);
           completarPerfil.update(dataUs,finalName,secondFinalName,finalLastName,
            $scope.userModel.email,$scope.userModel.documento,level,municipioLocal,$scope.userModel.placeCity,sex,$scope.userModel.cellPhone,
            municipioLocalp,asoChar,"","",
            act,cate,$scope.userModel.superfice,$scope.userModel.productionPricipal,
            municipioLocalname,
            $scope.userModel.SuperficieUltimoYear,
            $scope.userModel.NumeroFamiliares,
            $scope.userModel.TrabajadoresPermanentes,
            $scope.userModel.TrabajadoresTemporales,
            $scope.userModel.LugarDeProduccionName
            ).success(function(response){
               var datares=response;
               if(datares.Estado== 1) {
                resultUser=true;
                 $ionicLoading.hide();
                 console.log($scope.userModel);
                 console.log("HPPPPPP : "+totaData);
                 $localstorage.set('userName',totaData);
                 InfoService.createDataUser($scope.userModel);
                 //$state.go('app.login-result')
                 //window.location.reload(true); 
                 //history.go(0);
                 //window.location.href = window.location.href;
                 //$ionicHistory.go(0);
                  /*$state.go('app.homeclass', {}, {reload: false}).then(function(){
                       setTimeout(function() {
                        window.location.reload(true);
                    }, 500);
                 })*/
                 //$state.go('app.homeclass');
                 $scope.cleanStack();
               }
               else{
                 $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Resultado',
                  template: response.Texto
                });
              }
            }).error(function(response , status , headers ,config ,statusText) {
                $ionicLoading.hide()
                 $ionicPopup.alert({
                  title: 'Error de conexion',
                  template: 'No fue posible conectar al servidor, intentelo mas tarde.'
                });
            });
        }
        else
        {
          $ionicPopup.alert({
                title: 'Error en la validación de datos',
                template: 'Por favor, verifique sus datos e intente de nuevo'
            });
        }
      //}
        $ionicScrollDelegate.scrollTop();
        $scope.stateRegistre = true;
        $scope.stateSugesstion = false;
        $scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; ";
        $scope.activeBorderColorR = "border-color: #59595c; background-color: #5b9bfb; color: white; ";
        
    }

    $scope.actionButtonSugesstion = function(){
        $ionicScrollDelegate.scrollTop();
        $scope.stateRegistre = false;
        $scope.stateSugesstion = true;
        $scope.activeBorderColorS = "border-color: #2F6850; background-color: #5b9bfb; color: white; ";
        $scope.activeBorderColorR = "border-color: #59595c; background-color: #f2f2f2; color: #59595c; ";
    }   

    $scope.actionButtonSugesstionTab = function(){
        $ionicScrollDelegate.scrollTop();
        $scope.stateRegistre = true;
        $scope.stateSugesstion = false;
        $scope.activeBorderColorS = "border-color: #59595c; background-color: #f2f2f2; color: #59595c; ";
        $scope.activeBorderColorR =  "border-color: #2F6850; background-color: #5b9bfb; color: white; ";
    }      

    $scope.cleanStack = function(){     
        $ionicHistory.goBack(-3);
        $scope.userModel.registre = true;
        //console.log($scope.userModel)
        //InfoService.createDataUser($scope.userModel)
    }

  $scope.showPopup = function() {
  $scope.data = {};
  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: '<input type="password" placeholder="Ingrese su nueva clave" ng-model="data.password"> <br>'+
              '<input type="password" placeholder="Re ingrese la clave" ng-model="data.repassword">',
    title: 'Restablesca su clave',
    subTitle: '',
    scope: $scope,
    buttons: [
      { text: 'Cancelar' },
      {
        text: '<b>Recuperar</b>',
        type: 'button-positive',
        onTap: function(e) {
          if (angular.equals($scope.data.password,$scope.data.repassword)) {
            var dataUs=$localstorage.get('tokenUser')
            console.log("SETTING TOKEN "+dataUs)
            CambiarClave.update(dataUs,$scope.data.password,$scope.data.repassword).success(function(response){
                       var datares=response;
                       if(datares.Estado== 1) {
                         $ionicPopup.alert({
                             title: 'Resultado',
                             template: datares.Texto
                           });
                        }
                        else{
                          $ionicPopup.alert({
                             title: 'Resultado',
                             template: datares.Texto
                           });
                        }
                  }).error(function(data,status, headers, config) {
                               $ionicLoading.hide();
                               $ionicPopup.alert({
                                    title: 'Atención',
                                    template: 'Error de conexión, por favor intente nuevamente.'
                                }).then(function() {
                                  
                                }); });
            e.preventDefault();
          } else {
            $ionicPopup.alert({
                  title: 'Resultado',
                  template: "Las claves ingresadas no coinciden. Por favor rectifique e intente de nuevo"
                });
            }
          }
         }
        ]
      });
    }

});