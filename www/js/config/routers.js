angular.module('starter',
            ['ionic', 
             'starter.infoCtrl', 
             'starter.menuCtrl',
             'starter.ventaCtrl',
             'starter.DetailCtrl',
             'starter.favoriteCtrl',
             'ngCordova'
             ]).config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/");
        $stateProvider
        .state('app', {
               url: '/app',
               abstract: true,
               templateUrl: 'views/menu.html',
               controller: 'menuHomeCtrl'    
        })
        //------------------------------
        // ACCESSS
        //------------------------------
        .state ('acceso', {
                    url: '/',
                    templateUrl: 'views/home.html',
                    controller: "accessCtrl"
                    
        })
         .state ('aviso', {
                    url: '/aviso',
                    templateUrl: 'views/login/terminos.html',
        })
        //------------------------------
        //------------------------------
        // HOME
        //------------------------------
        .state('app.homeclass', {
               url: '/home',
               views: {
                   'menuContent': {
                       templateUrl: 'views/home/mainview.html',
                       controller: 'infoCtrl'        
                    }
                }
        })
        .state ('app.tabdetailclass', {
               cache: false,
               url: '/detail/:IdCompra',
               views: {
                    'menuContent': {
                     templateUrl: 'views/home/detail.html',
                     controller: 'DetailCtrl'        
               }
            }

        }) 
        .state ('app.tabdetailclasssipsa', {
               url: '/detailsipsa/:IdCompra',
               views: {
                    'menuContent': {
                     templateUrl: 'views/home/detailprecios.html',
                     controller: 'DetailPrecioCtrl'        
               }
            }

        }) 
        .state ('app.tabdetailclasscompra', {
               url: '/vercompra/:IdCompra',
               views: {
                    'menuContent': {
                     templateUrl: 'views/verpublicaciones/vercompra.html',
                     controller: 'verCompraCtrl'        
               }
            }

        }) 
        .state ('app.tabdetailclassventa', {
               url: '/verventa/:IdCompra',
               views: {
                    'menuContent': {
                     templateUrl: 'views/verpublicaciones/verventa.html',
                     controller: 'verVentaCtrl'        
               }
            }

        }) 
        .state ('app.publish', {
               url: '/publish',
               views: {
                    'menuContent': {
                     templateUrl: 'views/publicar/access.html',
                     controller: 'ventaCtrl'        
               }
            }

        }) 
        .state ('app.sold', {
              cache: false,
               url: '/sold',
               views: {
                    'menuContent': {
                     templateUrl: 'views/publicar/venta.html',
                     controller: 'ventaCtrl'        
               }
            }

        })
         .state ('app.buy', {
               cache:false,
               url: '/buy',
               views: {
                    'menuContent': {
                     templateUrl: 'views/publicar/compra.html',
                     controller: 'BuyCtrl'        
               }
            }

        })  
        //User
        .state('app.user', {
            url: '/user',
               views: {
                    'menuContent': {
                     templateUrl: 'views/profile/menup.html',
                     controller: "menuCtrl"         
               }
           }
        })
      //Favorite
      .state('app.favorite', {
        url: '/favorite',
        views: {
          'menuContent': {
             templateUrl: 'views/profile/favorite.html',
             controller: "favoriteCtrl"      
          }
        }
      })
      .state('app.mispublicaciones', {
        url: '/mispublicaciones',
        views: {
          'menuContent': {
             templateUrl: 'views/publiciones/mispublicaciones.html',
             controller: "misPublicacionesCtrl"      
          }
        }
      })
      //Login
      .state('app.login', {
        url: '/login',
        views: {
          'menuContent': {
             templateUrl: 'views/login/index.html',
             controller: 'logTestCtrl'     
          }
        }
      })
      //Result
      .state('app.login-result', {
        url: '/login/result',
        views: {
          'menuContent': {
             templateUrl: 'views/login/resultLogin.html',
             controller: 'logTestCtrl'     
          }
        }
      })
       .state('app.cambio-clave', {
        url: '/cambio/clave',
        views: {
          'menuContent': {
             templateUrl: 'views/login/cambioClave.html',
             controller: 'cambioClaveCtrl'     
          }
        }
      })
});